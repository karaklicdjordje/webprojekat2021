CREATE DATABASE IF NOT EXISTS knjizara_web;

use knjizara_web;

CREATE TABLE IF NOT EXISTS `knjiga` (
  `isbn` varchar(13) NOT NULL,
  `autor` varchar(45) NOT NULL,
  `broj_stranica` int(11) NOT NULL,
  `cena` double NOT NULL,
  `godina_izdavanja` int(11) NOT NULL,
  `izdavacka_kuca` varchar(45) NOT NULL,
  `jezik` varchar(45) NOT NULL,
  `kratak_opis` varchar(120) DEFAULT NULL,
  `naziv` varchar(45) NOT NULL,
  `pismo` int(11) DEFAULT NULL,
  `povez` int(11) DEFAULT NULL,
  `prosecna_ocena_knjige` double DEFAULT NULL,
  `slika` longblob,
  `broj_primeraka` int(11) DEFAULT NULL,
  PRIMARY KEY (`isbn`),
  UNIQUE KEY `isbn_UNIQUE` (`isbn`)
);


CREATE TABLE IF NOT EXISTS `zanr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) NOT NULL,
  `opis` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_5017h21si8nmbu7nwjqu9xkki` (`ime`)
) ;


CREATE TABLE IF NOT EXISTS `korisnik` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adresa` varchar(70) NOT NULL,
  `telefon` varchar(20) NOT NULL,
  `datum_rodjenja` date NOT NULL,
  `email` varchar(45) NOT NULL,
  `ime` varchar(20) NOT NULL,
  `korisnicko_ime` varchar(20) NOT NULL,
  `lozinka` varchar(120) NOT NULL,
  `prezime` varchar(20) NOT NULL,
  `uloga` int(11) NOT NULL,
  `vreme_registracije` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_email` (`email`),
  UNIQUE KEY `UK_korisnicko_ime` (`korisnicko_ime`)
);



CREATE TABLE IF NOT EXISTS `knjiga_zanr` (
  `knjiga_id` varchar(13) NOT NULL,
  `zanr_id` bigint(20) NOT NULL,
  PRIMARY KEY (`knjiga_id`,`zanr_id`),
  KEY `FK_knjiga_zanr_zanr_id` (`zanr_id`),
  KEY `FK_knjiga_zanr_knjiga_id` (`knjiga_id`),
  CONSTRAINT `FK_knjiga_zanr_zanr_id` FOREIGN KEY (`zanr_id`) REFERENCES `zanr` (`id`),
  CONSTRAINT `FK_knjiga_zanr_knjiga_id` FOREIGN KEY (`knjiga_id`) REFERENCES `knjiga` (`isbn`)
);


CREATE TABLE IF NOT EXISTS `korpa` (
  `broj_primeraka` int(11) DEFAULT NULL,
  `cena` double DEFAULT NULL,
  `knjiga_id` varchar(13) NOT NULL,
  `korisnik_id` bigint(20) NOT NULL,
  PRIMARY KEY (`knjiga_id`, `korisnik_id`),
  KEY `FK_korpa_knjiga_id` (`knjiga_id`),
  KEY `FK_korpa_korisnik_id` (`korisnik_id`),
  CONSTRAINT `FK_korpa_knjiga_id` FOREIGN KEY (`knjiga_id`) REFERENCES `knjiga` (`isbn`),
  CONSTRAINT `FK_korpa_korisnik_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`)
);


CREATE TABLE IF NOT EXISTS `lista_zelja` (
  `knjiga_id` varchar(13) NOT NULL,
  `korisnik_id` bigint(20) NOT NULL,
  PRIMARY KEY (`knjiga_id`, `korisnik_id`),
  KEY `FK_lista_zelja_knjiga_id` (`knjiga_id`),
  KEY `FK_lista_zelja_korisnik_id` (`korisnik_id`),
  CONSTRAINT `FK_lista_zelja_knjiga_id` FOREIGN KEY (`knjiga_id`) REFERENCES `knjiga` (`isbn`),
  CONSTRAINT `FK_lista_zelja_korisnik_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`)
);


CREATE TABLE IF NOT EXISTS `loyalty_kartica` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `broj_poena` int(11) DEFAULT NULL,
  `popust` double DEFAULT NULL,
  `status_kartice` int(11) DEFAULT NULL,
  `korisnik_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_loyalty_korisnik_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`)
);


CREATE TABLE IF NOT EXISTS `kupovina` (
  `kupovina_id` BIGINT(11) NOT NULL AUTO_INCREMENT,
  `ukupna_cena_kupovine` double DEFAULT NULL,
  `vreme_kupovine` datetime(6) NOT NULL,
  `korisnik_id` bigint(20) NOT NULL,
  `broj_kupljenih_knjiga` INT,
  PRIMARY KEY (`kupovina_id`),
  CONSTRAINT `FK_kupovina_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`)
);


CREATE TABLE IF NOT EXISTS `kupljena_knjiga` (
  `kupljena_knjiga_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `broj_primeraka` INT(11) DEFAULT NULL,
  `cena` DOUBLE DEFAULT NULL,
  `knjiga_id` VARCHAR(13) NOT NULL,
  `korisnik_id` BIGINT(20) NOT NULL,
  `kupovina_id` BIGINT(20) NOT NULL,
  PRIMARY KEY (`kupljena_knjiga_id`),
  CONSTRAINT `FK_kupljena_knjiga_knjiga_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`),
  CONSTRAINT `FK_kupljena_knjiga_ko` FOREIGN KEY (`knjiga_id`) REFERENCES `knjiga` (`isbn`),
  CONSTRAINT `FK_kupljena_knjiga_kup` FOREIGN KEY (`kupovina_id`) REFERENCES `kupovina` (`kupovina_id`)
);

CREATE TABLE IF NOT EXISTS `komentar` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `tekst_komentara` VARCHAR(255) NOT NULL,
  `ocena` INT NOT NULL,
  `knjiga_id` VARCHAR(13) NOT NULL,
  `korisnik_id` BIGINT(20) NOT NULL,
  `datum_komentara` DATE NOT NULL,
  `status_komentara` INT(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_komentar_knjiga_id` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`),
  CONSTRAINT `FK_komentar_korisnik_id` FOREIGN KEY (`knjiga_id`) REFERENCES `knjiga` (`isbn`)
);


INSERT INTO `knjizara_web`.`korisnik` (`adresa`, `telefon`, `datum_rodjenja`, `email`, `ime`, `korisnicko_ime`, `lozinka`, `prezime`, `uloga`, `vreme_registracije`) VALUES ('Maskima Gorkog 15 Sremska Mitrovica', '064/11224456', '1997-11-22', 'karaklicdjorjde@gmail.com', 'Djordje', 'djordje', '$2a$10$bnQLtP.gkS48siNK7FAlJ.E/fjlBKmUcRZuu1Y4nr6FDEDWtw07DW', 'Karaklic', '1', '2021-01-22');
INSERT INTO `knjizara_web`.`korisnik` (`adresa`, `telefon`, `datum_rodjenja`, `email`, `ime`, `korisnicko_ime`, `lozinka`, `prezime`, `uloga`, `vreme_registracije`) VALUES ('Bulevar oslobodjenja 221 Novi Sad', '065/22234456', '1993-05-14', 'steva@gmail.com', 'Steva', 'steva', '$2a$10$KDLBhR7HgEZYqJfbLSRDy.YOvA4b/zOuCf.CxCL4hxgGoJ9YQp9gO', 'Stevanovic', '0', '2021-02-07');
INSERT INTO `knjizara_web`.`korisnik` (`adresa`, `telefon`, `datum_rodjenja`, `email`, `ime`, `korisnicko_ime`, `lozinka`, `prezime`, `uloga`, `vreme_registracije`) VALUES ('Krunska 11 Beograd', '065/1415123', '1987-09-11', 'jova@gmail.com', 'Jovan', 'jovan', '$2a$10$do5i0Ae76GzlCiRfS6a1eOnods.eZSShqW0mEwQ0uUS2UDdARlQky', 'Jovanovic', '0', '2021-02-03');
INSERT INTO `knjizara_web`.`korisnik` (`adresa`, `telefon`, `datum_rodjenja`, `email`, `ime`, `korisnicko_ime`, `lozinka`, `prezime`, `uloga`, `vreme_registracije`) VALUES ('Glavna 54 Ruma', '065/1719556', '1998-06-23', 'petar@gmail.com', 'Petar', 'petar', '$2a$10$4Le4d5H.rFiP3BSuuvD74O8uv2o7ORpVdWJwHi0cpQm01OFgGaOee', 'Petrovic', '0', '2021-01-31');
INSERT INTO `knjizara_web`.`korisnik` (`adresa`, `telefon`, `datum_rodjenja`, `email`, `ime`, `korisnicko_ime`, `lozinka`, `prezime`, `uloga`, `vreme_registracije`) VALUES ('Bulevar oslobodjenja 91 Novi Sad', '060/8758578', '1993-05-14', 'jelena@gmail.com', 'Jelena', 'jelena', '$2a$10$XGLmtTGx6/QEQ2tOH.Jz6OnI8v9ubN7J/fxY.zzetamnyu.y58Muy', 'Jelenkovic', '0', '2021-02-05');
INSERT INTO `knjizara_web`.`korisnik` (`adresa`, `telefon`, `datum_rodjenja`, `email`, `ime`, `korisnicko_ime`, `lozinka`, `prezime`, `uloga`, `vreme_registracije`) VALUES ('Janka Veselinovica 25  Sid', '061/3546877', '1995-12-12', 'ivana@gmail.com', 'Ivana', 'ivana', '$2a$2a$2a$10$FAgMEAcWOOYFSKn7us16/eHC.Zfr7m70mI3gMDAUsNBveuyQUw1pq', 'Ivanovic', '0', '2021-02-07');
INSERT INTO `knjizara_web`.`korisnik` (`adresa`, `telefon`, `datum_rodjenja`, `email`, `ime`, `korisnicko_ime`, `lozinka`, `prezime`, `uloga`, `vreme_registracije`) VALUES ('Oblakovska 130 Beograd', '065/4545464', '1992-10-14', 'marko@gmail.com', 'Marko', 'marko', '$2a$10$coxHEa6w9BbSURIk9Y/S3eEiWPlQdA1jH0DtmSzvr1PbiCMYRXOTS', 'Markovic', '0', '2020-12-11');
INSERT INTO `knjizara_web`.`korisnik` (`adresa`, `telefon`, `datum_rodjenja`, `email`, `ime`, `korisnicko_ime`, `lozinka`, `prezime`, `uloga`, `vreme_registracije`) VALUES ('Milosa Obilica 11 Sremska Mitrovica', '064/3573578', '1997-03-03', 'janko@gmail.com', 'Janko', 'janko', '$2a$10$LLYATDrirmqe39OHxog4T.GahXPWKsCqE4JpOKmpvt6mv2UGtTDla', 'Jankovic', '0', '2020-09-27');

INSERT INTO `knjizara_web`.`zanr` (`ime`, `opis`) VALUES ('Epika', 'Kazivanje o nekom dogadjaju, zbivanju ili licnosti');
INSERT INTO `knjizara_web`.`zanr` (`ime`, `opis`) VALUES ('Kompjuteri i internet', 'Edukacija za rad na racunaru, softveru ili hardveru');
INSERT INTO `knjizara_web`.`zanr` (`ime`, `opis`) VALUES ('Edukacija', 'Edukativna knjiga za sticanje znanja');
INSERT INTO `knjizara_web`.`zanr` (`ime`, `opis`) VALUES ('Decija', 'Knjiga namenjena decijem uzrastu');
INSERT INTO `knjizara_web`.`zanr` (`ime`, `opis`) VALUES ('Drama', 'Razvoj likova koji se suocavaju sa emocionalnim temama');
INSERT INTO `knjizara_web`.`zanr` (`ime`, `opis`) VALUES ('Istorija', 'Opis istorijskog dogadjaja ili licnosti');
INSERT INTO `knjizara_web`.`zanr` (`ime`, `opis`) VALUES ('Avantura', 'Pustolovine i iskustva koja ukljucuju nepredvidive dogadjaje');
INSERT INTO `knjizara_web`.`zanr` (`ime`, `opis`) VALUES ('Poezija', 'Pesnikova poruka izrecena kroz stihove');

INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('2020202020202', 'Ranga Rao Karanam', '475', '2420', '2017', 'Kompjuter biblioteka', 'Srpski', 'Sveobuhvatni vodic za radni okvir Spring', 'Spring 5', '1', '1', '4.45', '45');
INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('3532363532363', 'Neil Smyth', '475', '2420.00', '2017', 'Kompjuter biblioteka', 'Srpski', 'Azurirana knjiga za Androdi Studio 3.2 i Android 9', 'Android 9', '0', '0', '3.44', '21');
INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('2421252623522', 'Herbert Schildt', '475', '1982.00', '2019', 'Mikro Knjiga', 'Engleski', 'Java kompletan prirucnik', 'Java', '1', '1', '4.45', '45');
INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('4215212352127', 'Simon Mellor', '314', '1125.00', '1995', 'Campaign', 'Engleski', 'Student\'s book for engish', 'English for the military', '0', '0', '3.54', '24');
INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('7121244567895', 'Joyce Hannam', '112', '522.00', '1991', 'Oxford', 'Engleski', 'One woman\'s tale of secret love, adventure and escape', 'Ariadne\'s story', '0', '0', '2.78', '45');
INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('1314131516171', 'Petar Petrovic Njegos', '238', '534.60', '2001', 'Cigoja', 'Srpski', 'Istraga poturica u Crnoj Gori', 'Gorski vijenac', '1', '1', '4.44', '78');
INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('5544662354123', 'Suzana Tamaro', '97', '522.00', '1991', 'Odiseja', 'Srpski', 'Uzbudljiva prica koja na svakoj stranici nudi skok u novu avanturu.', 'Skaci Barte', '1', '0', '3.11', '34');
INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('5987662354123', 'Meri Bird', '504', '1070.00', '2021', 'Laguna', 'Srpski', 'Sveobuhvatna istorija Rimskog carstva.', 'SPQR Istorija starog Rima', '0', '1', '2.77', '23');
INSERT INTO `knjizara_web`.`knjiga` (`isbn`, `autor`, `broj_stranica`, `cena`, `godina_izdavanja`, `izdavacka_kuca`, `jezik`, `kratak_opis`, `naziv`, `pismo`, `povez`, `prosecna_ocena_knjige`, `broj_primeraka`) VALUES ('7514567894562', 'Rupi Kaur', '504', '1736.11', '2021', 'Harper Collins Publishers Ltd', 'Engleski', 'Journey of wilting, falling, rooting, rising and blooming', 'The sun and her flowers', '0', '1', '2.87', '56');

SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Spring 5');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Kompjuteri i internet');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Android 9');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Kompjuteri i internet');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Android 9');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Edukacija');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Java');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Kompjuteri i internet');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='English for the military');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Edukacija');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Gorski vijenac');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Epika');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Skaci Barte');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Avantura');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='SPQR Istorija starog Rima');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Istorija');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Edukacija');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='The sun and her flowers');
SET @zanr_id = (SELECT id FROM zanr WHERE ime = 'Poezija');
INSERT INTO `knjizara_web`.`knjiga_zanr` (`knjiga_id`, `zanr_id`)VALUES(@isbn, @zanr_id);


SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
INSERT INTO `knjizara_web`.`loyalty_kartica` (`broj_poena`, `status_kartice`, `korisnik_id`) VALUES ('5', '1', @korisnik_id);
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'marko@gmail.com');
INSERT INTO `knjizara_web`.`loyalty_kartica` (`broj_poena`, `status_kartice`, `korisnik_id`) VALUES ('5', '0', @korisnik_id);
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
INSERT INTO `knjizara_web`.`loyalty_kartica` (`broj_poena`, `status_kartice`, `korisnik_id`) VALUES ('15', '1', @korisnik_id);
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'ivana@gmail.com');
INSERT INTO `knjizara_web`.`loyalty_kartica` (`broj_poena`, `status_kartice`, `korisnik_id`) VALUES ('4', '2', @korisnik_id);



SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
INSERT INTO `knjizara_web`.`kupovina` (`ukupna_cena_kupovine`, `vreme_kupovine`, `korisnik_id`, `broj_kupljenih_knjiga`) VALUES ('68012', '2021-01-22', @korisnik_id, '31');	
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'marko@gmail.com');
INSERT INTO `knjizara_web`.`kupovina` (`ukupna_cena_kupovine`, `vreme_kupovine`, `korisnik_id`, `broj_kupljenih_knjiga`) VALUES ('16170', '2021-01-25', @korisnik_id, '12');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
INSERT INTO `knjizara_web`.`kupovina` (`ukupna_cena_kupovine`, `vreme_kupovine`, `korisnik_id`, `broj_kupljenih_knjiga`) VALUES ('15247', '2021-01-28', @korisnik_id, '11');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'petar@gmail.com');
INSERT INTO `knjizara_web`.`kupovina` (`ukupna_cena_kupovine`, `vreme_kupovine`, `korisnik_id`, `broj_kupljenih_knjiga`) VALUES ('25325', '2021-02-01', @korisnik_id, '21');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
INSERT INTO `knjizara_web`.`kupovina` (`ukupna_cena_kupovine`, `vreme_kupovine`, `korisnik_id`, `broj_kupljenih_knjiga`) VALUES ('13204', '2021-02-04', @korisnik_id, '9');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'ivana@gmail.com');
INSERT INTO `knjizara_web`.`kupovina` (`ukupna_cena_kupovine`, `vreme_kupovine`, `korisnik_id`, `broj_kupljenih_knjiga`) VALUES ('10732.22', '2021-02-07', @korisnik_id, '5');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'jelena@gmail.com');
INSERT INTO `knjizara_web`.`kupovina` (`ukupna_cena_kupovine`, `vreme_kupovine`, `korisnik_id`, `broj_kupljenih_knjiga`) VALUES ('13750', '2021-02-08', @korisnik_id, '7');				


SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Spring 5');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-22');
SET @brojPrimeraka = 7;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Android 9');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-22');
SET @brojPrimeraka = 8;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Java');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-22');
SET @brojPrimeraka = 16;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'marko@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='The sun and her flowers');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-25');
SET @brojPrimeraka = 5;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'marko@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='SPQR Istorija starog Rima');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-25');
SET @brojPrimeraka = 7;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Gorski vijenac');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-28');
SET @brojPrimeraka = 3;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);


SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Spring 5');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-28');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Java');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-28');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Android 9');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-28');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='SPQR Istorija starog Rima');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-01-28');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'petar@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='SPQR Istorija starog Rima');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-01');
SET @brojPrimeraka = 3;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'petar@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Skaci Barte');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-01');
SET @brojPrimeraka = 4;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'petar@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='The sun and her flowers');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-01');
SET @brojPrimeraka = 7;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'petar@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='English for the military');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-01');
SET @brojPrimeraka = 7;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='English for the military');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-04');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Ariadne\'s story');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-04');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Java');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-04');
SET @brojPrimeraka = 5;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'ivana@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Android 9');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-07');
SET @brojPrimeraka = 3;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'ivana@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='The sun and her flowers');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-07');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'jelena@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Ariadne\'s story');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-08');
SET @brojPrimeraka = 4;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'jelena@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Spring 5');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-08');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'jelena@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Android 9');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-08');
SET @brojPrimeraka = 2;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'jelena@gmail.com');
SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Java');
SET @kupovina_id = (SELECT kupovina_id FROM kupovina WHERE korisnik_id=@korisnik_id and vreme_kupovine='2021-02-08');
SET @brojPrimeraka = 1;
SET @cena = (SELECT cena FROM knjiga WHERE isbn=@isbn) * @brojPrimeraka;
INSERT INTO `knjizara_web`.`kupljena_knjiga` (`broj_primeraka`, `cena`, `knjiga_id`, `korisnik_id`, `kupovina_id`) VALUES (@brojPrimeraka, @cena, @isbn, @korisnik_id, @kupovina_id);

SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Java');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
INSERT INTO `knjizara_web`.`komentar` (`tekst_komentara`, `ocena`, `knjiga_id`, `korisnik_id`, `datum_komentara`, `status_komentara`) VALUES ('Knjiga je odlicna.', '4', @isbn, @korisnik_id, '2021-01-11', '1');

SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Spring 5');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
INSERT INTO `knjizara_web`.`komentar` (`tekst_komentara`, `ocena`, `knjiga_id`, `korisnik_id`, `datum_komentara`, `status_komentara`) VALUES ('Fantasticno 5+.', '5', @isbn, @korisnik_id, '2021-01-15', '0');

SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Android 9');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'steva@gmail.com');
INSERT INTO `knjizara_web`.`komentar` (`tekst_komentara`, `ocena`, `knjiga_id`, `korisnik_id`, `datum_komentara`, `status_komentara`) VALUES ('Knjiga je bezveze.', '2', @isbn, @korisnik_id, '2021-01-21', '2');

SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Java');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
INSERT INTO `knjizara_web`.`komentar` (`tekst_komentara`, `ocena`, `knjiga_id`, `korisnik_id`, `datum_komentara`, `status_komentara`) VALUES ('Knjiga je odlicna.', '5', @isbn, @korisnik_id, '2021-02-08', '1');

SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Spring 5');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'janko@gmail.com');
INSERT INTO `knjizara_web`.`komentar` (`tekst_komentara`, `ocena`, `knjiga_id`, `korisnik_id`, `datum_komentara`, `status_komentara`) VALUES ('Odlicno je opisan Spring framework. Moja preporuka.', '4', @isbn, @korisnik_id, '2021-02-02', '1');

SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='Skaci Barte');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'petar@gmail.com');
INSERT INTO `knjizara_web`.`komentar` (`tekst_komentara`, `ocena`, `knjiga_id`, `korisnik_id`, `datum_komentara`, `status_komentara`) VALUES ('Zanimljivo. Prilagodjeno deci.', '4', @isbn, @korisnik_id, '2021-01-30', '0');

SET @isbn = (SELECT isbn FROM knjiga WHERE naziv='The sun and her flowers');
SET @korisnik_id = (SELECT id FROM korisnik WHERE email = 'petar@gmail.com');
INSERT INTO `knjizara_web`.`komentar` (`tekst_komentara`, `ocena`, `knjiga_id`, `korisnik_id`, `datum_komentara`, `status_komentara`) VALUES ('Izuzetno.', '4', @isbn, @korisnik_id, '2021-01-31', '1');