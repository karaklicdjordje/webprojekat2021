$(document).ready(function () {
	$("#pretraga").on("keyup", function () {
			var value = $(this).val().toLowerCase();
			$("#knjigeTabela tr").filter(function () {
					$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
				});
		});
		
		var $cena = $("#knjigeTabela").find(".cena");
		
		$('#brojPoena').change(function(){	
			$procenat = $(this).val() * 5;
			
			$('#knjigeTabela  > tr >td.cena').each(function() {
				$trenutnaCena = $(this).text();
				$(this).text($trenutnaCena - $trenutnaCena*$procenat/100);
			});
			
		});	
	
	$(".btn-obrisi").on("click", function(e) {
		var tableRow = $(this).closest("tr");
				var korisnikId = $(this).closest("tr").find(".korisnikId").text();
				var knjigaId = $(this).closest("tr").find(".knjigaId").text();

				$.ajax({
					url : `/korpa/brisanje?korisnikId=${korisnikId}&knjigaId=${knjigaId}`,
					 type: 'GET',
					dataType: 'json',
				contentType:'application/json',
					complete: function(data){
					tableRow.remove();
		                }
				});
		});
	
	$(".btn-obrisi-lista-zelja").on("click", function(e) {
		var tableRow = $(this).closest("tr");
				var korisnikId = $(this).closest("tr").find(".korisnikId").text();
				var knjigaId = $(this).closest("tr").find(".knjigaId").text();

				$.ajax({
					url : `/listaZelja/brisanje?korisnikId=${korisnikId}&knjigaId=${knjigaId}`,
					 type: 'GET',
					dataType: 'json',
				contentType:'application/json',
					complete: function(data){
					tableRow.remove();
		                }
				});
		});
	});