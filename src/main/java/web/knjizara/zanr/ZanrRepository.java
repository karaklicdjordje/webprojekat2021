package web.knjizara.zanr;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import web.knjizara.dao.Dao;

@Repository
public class ZanrRepository implements Dao<Zanr, Long>{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private class ZanrRowMapper implements RowMapper<Zanr> {

		@Override
		public Zanr mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			Long id = rs.getLong("id");
			String imeZanra = rs.getString("ime");
			String opisZanra = rs.getString("opis");
			Zanr zanr = new Zanr();
			zanr.setId(id);
			zanr.setIme(imeZanra);
			zanr.setOpis(opisZanra);
			return zanr;
		}

	}

	@Override
	public Optional<Zanr> findById(Long id) {
		String sql = "SELECT * FROM zanr WHERE id=?";
		Zanr zanr = jdbcTemplate.query(sql, new ZanrRowMapper(), id).get(0);
		
		return Optional.ofNullable(zanr);
	}

	@Override
	public List<Zanr> findAll() {
		String sql = "SELECT * FROM zanr";
		return jdbcTemplate.query(sql, new ZanrRowMapper());
	}

	@Override
	public void save(Zanr t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Zanr t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Zanr t) {
		// TODO Auto-generated method stub
		
	}
	
	

}
