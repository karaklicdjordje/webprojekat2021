package web.knjizara.zanr;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import web.knjizara.knjiga.Knjiga;

public class Zanr {
	
    private Long id;
 
    private String ime;
     
    private String opis;
    
    private Set<Knjiga> knjige = new HashSet<>();
    
	public Zanr(Long id, String ime, String opis, Set<Knjiga> knjige) {
		super();
		this.id = id;
		this.ime = ime;
		this.opis = opis;
		this.knjige = knjige;
	}
	
	public Zanr() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Set<Knjiga> getKnjige() {
		return knjige;
	}
	
}
