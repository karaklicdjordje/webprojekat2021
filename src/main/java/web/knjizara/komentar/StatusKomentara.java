package web.knjizara.komentar;

public enum StatusKomentara {
	
	NA_CEKANJU, ODOBREN, NIJE_ODOBREN;
	
	public static StatusKomentara vratiStatusKomentaraByInt(int broj) {
		switch (broj) {
		case 0:
			 return StatusKomentara.NA_CEKANJU;
		case 1:
			return StatusKomentara.ODOBREN;
		case 2:
			return StatusKomentara.NIJE_ODOBREN;
		default:
			return null;
		}
	}

}
