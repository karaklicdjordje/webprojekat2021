package web.knjizara.komentar;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import web.knjizara.dao.KomentarDAO;

@Repository
public class KomentarRepository implements KomentarDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KomentarRowMapper implements RowMapper<Komentar> {

		@Override
		public Komentar mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long id = rs.getLong("id");
			Long korisnikId = rs.getLong("korisnik_id");
			String knjigaId = rs.getString("knjiga_id");
			String tekstKomentara = rs.getString("tekst_komentara");
			Integer ocena = rs.getInt("ocena");
			StatusKomentara statusKomentara = StatusKomentara.vratiStatusKomentaraByInt(rs.getInt("status_komentara"));
			Date datumKomentara = rs.getDate("datum_komentara");
			Komentar komentar = new Komentar();
			komentar.setId(id);
			komentar.setKorisnikId(korisnikId);
			komentar.setKnjigaId(knjigaId);
			komentar.setStatusKomentara(statusKomentara);
			komentar.setTekstKomentara(tekstKomentara);
			komentar.setOcena(ocena);
			komentar.setDatumKomentara(datumKomentara);
			return komentar;
		}

	}

	@Override
	public Optional<Komentar> findById(Long id) {
		try {
			String sql = "SELECT * FROM komentar WHERE id=?";
			Komentar komentar = jdbcTemplate.query(sql, new KomentarRowMapper(), id).get(0);
			return Optional.ofNullable(komentar);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Komentar> findAll() {
		String sql = "SELECT * FROM komentar";
		return jdbcTemplate.query(sql, new KomentarRowMapper());
	}

	@Override
	public Komentar findByKorisnikId(Long korisnikId) {
		try {
			String sql = "SELECT * FROM komentar WHERE korisnik_id=?";
			return jdbcTemplate.query(sql, new KomentarRowMapper(), korisnikId).get(0);
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public void save(Komentar komentar) {
		String sql = "INSERT INTO komentar(knjiga_id, korisnik_id, tekst_komentara, datum_komentara, ocena, status_komentara) " + "VALUES('"
				+ komentar.getKnjigaId() + "','" + komentar.getKorisnikId() + "','" + komentar.getTekstKomentara() + "','" +
				komentar.getDatumKomentara() + "','" +komentar.getOcena() + "','" + komentar.getStatusKomentara().ordinal() + "')";

		jdbcTemplate.update(sql);
	}

	@Override
	public void update(Komentar komentar) {
		String sql = "UPDATE komentar SET ocena =?, status_komentara=? WHERE id=?";
		jdbcTemplate.update(sql, komentar.getOcena(),
				komentar.getStatusKomentara().ordinal(), komentar.getId());
	}

	@Override
	public void delete(Komentar komentar) {
		String sql = "DELETE FROM komentar WHERE id=?";
		jdbcTemplate.update(sql, komentar.getId());
	}

	@Override
	public List<Komentar> findAllByStatusKomentara(StatusKomentara statusKomentara) {
		String sql = "SELECT * FROM komentar WHERE status_komentara=?";
		return jdbcTemplate.query(sql, new KomentarRowMapper(), statusKomentara.ordinal());
	}

}
