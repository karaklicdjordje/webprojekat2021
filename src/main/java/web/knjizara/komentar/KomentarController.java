package web.knjizara.komentar;

import java.sql.Date;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import web.knjizara.knjiga.Knjiga;
import web.knjizara.knjiga.KnjigaRepository;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;
import web.knjizara.kupljenaknjiga.KupljenaKnjiga;
import web.knjizara.kupljenaknjiga.KupljenaKnjigaRepository;
import web.knjizara.kupovina.Kupovina;
import web.knjizara.kupovina.KupovinaRepository;

@Controller
public class KomentarController {

	@Autowired
	KomentarRepository komentarRepository;

	@Autowired
	KorisnikRepository korisnikRepository;

	@Autowired
	KnjigaRepository knjigaRepository;

	@Autowired
	KupljenaKnjigaRepository kupljenaKnjigaRepository;
	
	@Autowired
	KupovinaRepository kupovinaRepository;

	@GetMapping("/admin/komentari")
	public String prikaziKomentareNaCekanju(Model model) {
		List<Komentar> listaKomentara = komentarRepository.findAllByStatusKomentara(StatusKomentara.NA_CEKANJU);
		Map<Komentar, Korisnik> mapaKomentara = new LinkedHashMap<>();
		Collections.sort(listaKomentara);

		for (Komentar komentar : listaKomentara) {
			Korisnik korisnik = korisnikRepository.findById(komentar.getKorisnikId()).get();
			mapaKomentara.put(komentar, korisnik);
		}
		model.addAttribute("mapaKomentara", mapaKomentara);
	
		return "komentari";
	}

	@GetMapping("/admin/komentarStatus")
	public void azurirajKomentar(@RequestParam(name = "komentarId") Long id,
			@RequestParam(name = "statusKomentara") Integer statusKomentara) {
		Komentar komentar = komentarRepository.findById(id).get();
//		int status = Integer.parseInt(statusKomentara);
		switch (statusKomentara) {
		case 1:
			komentar.setStatusKomentara(StatusKomentara.ODOBREN);
			break;
		case 2:
			komentar.setStatusKomentara(StatusKomentara.NIJE_ODOBREN);
		default:
			break;
		}
		komentarRepository.update(komentar);
		Knjiga knjiga = knjigaRepository.findById(komentar.getKnjigaId()).get();
		knjiga.setProsecnaOcenaKnjige((knjiga.getProsecnaOcenaKnjige()+komentar.getOcena())/2);
		knjigaRepository.update(knjiga);
	}

	@GetMapping("/knjiga/komentar/{isbn}")
	public String prikaziKomentareZaKnjigu(Model model, @PathVariable String isbn) {
		Korisnik korisnik = korisnikRepository.findKorisnikByKorisnikDetails();
		Knjiga knjiga = knjigaRepository.findById(isbn).get();
		List<Kupovina> listaKupovina = kupovinaRepository.findAllByKorisnikId(korisnik.getId());
		for (Kupovina kupovina : listaKupovina) {
			if(korisnik.isKupljenaKnjiga()) {
				break;
			}
			List<KupljenaKnjiga> listaKupljenjihKnjiga = kupljenaKnjigaRepository.findAllByKupovinaId(kupovina.getKupovinaId());
			for (KupljenaKnjiga kupljenaKnjiga : listaKupljenjihKnjiga) {
				if(kupljenaKnjiga.getKnjiga().getIsbn().equals(isbn)) {
					korisnik.setKupljenaKnjiga(true);
					break;
				}
			}
		}
		
		model.addAttribute("korisnik", korisnik);
		model.addAttribute("knjiga", knjiga);
		Map<Komentar, Korisnik> mapaKomentara = new TreeMap<Komentar, Korisnik>();
		List<Komentar> listaKomentara = komentarRepository.findAllByStatusKomentara(StatusKomentara.ODOBREN);
		for (Komentar komentar : listaKomentara) {
			if (komentar.getKnjigaId().equals(isbn)) {
				Korisnik autor = korisnikRepository.findById(komentar.getKorisnikId()).get();
				mapaKomentara.put(komentar, autor);
			}
		}
		model.addAttribute("mapaKomentara", mapaKomentara);
		Komentar komentar = new Komentar();
		model.addAttribute("komentar", komentar);
		return "komentari_knjiga";
	}

	@PostMapping("/komentar/dodavanje")
	public String dodajKomentar(Model model, Komentar komentar) {
		komentar.setStatusKomentara(StatusKomentara.NA_CEKANJU);
		Date datumKomentara = new Date(System.currentTimeMillis());
		komentar.setDatumKomentara(datumKomentara);
		
		komentarRepository.save(komentar);

		String successMessage = "Hvala na izdvojenom vremenu! Vas komentar bice prosledjen na odobrenje administratoru";

		System.out.println(successMessage);
		model.addAttribute("successMessage", successMessage);

		return prikaziKomentareZaKnjigu(model, komentar.getKnjigaId());
	}

}
