package web.knjizara.komentar;

import java.sql.Date;

public class Komentar implements Comparable<Komentar>{

	private Long id;

	private String tekstKomentara;

	private Integer ocena;

	private Long korisnikId;

	private String knjigaId;
	
	private Date datumKomentara;

	private StatusKomentara statusKomentara;

	
	public Komentar(Long id, String tekstKomentara, Integer ocena, Long korisnikId, String knjigaId,
			Date datumKomentara, StatusKomentara statusKomentara) {
		super();
		this.id = id;
		this.tekstKomentara = tekstKomentara;
		this.ocena = ocena;
		this.korisnikId = korisnikId;
		this.knjigaId = knjigaId;
		this.datumKomentara = datumKomentara;
		this.statusKomentara = statusKomentara;
	}

	public Komentar() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTekstKomentara() {
		return tekstKomentara;
	}

	public void setTekstKomentara(String tekstKomentara) {
		this.tekstKomentara = tekstKomentara;
	}

	public Integer getOcena() {
		return ocena;
	}

	public void setOcena(Integer ocena) {
		this.ocena = ocena;
	}

	public Long getKorisnikId() {
		return korisnikId;
	}

	public void setKorisnikId(Long korisnikId) {
		this.korisnikId = korisnikId;
	}

	public String getKnjigaId() {
		return knjigaId;
	}

	public void setKnjigaId(String knjigaId) {
		this.knjigaId = knjigaId;
	}

	public StatusKomentara getStatusKomentara() {
		return statusKomentara;
	}

	public void setStatusKomentara(StatusKomentara statusKomentara) {
		this.statusKomentara = statusKomentara;
	}

	public Date getDatumKomentara() {
		return datumKomentara;
	}

	public void setDatumKomentara(Date datumKomentara) {
		this.datumKomentara = datumKomentara;
	}

	@Override
	public int compareTo(Komentar komentar) {
		return datumKomentara.compareTo(komentar.datumKomentara);
	}
	
	

}
