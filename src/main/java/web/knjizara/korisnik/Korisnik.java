package web.knjizara.korisnik;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import web.knjizara.kupljenaknjiga.KupljenaKnjiga;

public class Korisnik {
     
    private Long id;
    
    private String korisnickoIme;
    
    private String lozinka;
     
    private String email;
    
    private String ime;
    
    private String prezime;
    
    private Date datumRodjenja;

    private String adresa;
    
    private String brojTelefona;
    
    private LocalDateTime vremeRegistracije;
    
    private Uloga uloga;
    
    private boolean hasLoyaltyKartica;
    
    private boolean isLoyaltyKarticaNaCekanju;
    
    private boolean isKupljenaKnjiga;

	private Set<KupljenaKnjiga> kupljenaKnjiga = new HashSet<>();
    

	public Korisnik(Long id, String korisnickoIme, String lozinka, String email, String ime, String prezime,
			Date datumRodjenja, String adresa, String brojTelefona) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumRodjenja = new java.sql.Date(datumRodjenja.getDate());
		this.adresa = adresa;
		this.brojTelefona = brojTelefona;
	}
	
	public Korisnik() {
		super();
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Date getDatumRodjenja() {
		return datumRodjenja;
	}

	public void setDatumRodjenja(Date datumRodjenja) {
		this.datumRodjenja = datumRodjenja;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public LocalDateTime getVremeRegistracije() {
		return vremeRegistracije;
	}

	public void setVremeRegistracije(LocalDateTime vremeRegistracije) {
		this.vremeRegistracije = vremeRegistracije;
	}

	public Uloga getUloga() {
		return uloga;
	}

	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}

	public boolean isHasLoyaltyKartica() {
		return hasLoyaltyKartica;
	}

	public void setHasLoyaltyKartica(boolean hasLoyaltyKartica) {
		this.hasLoyaltyKartica = hasLoyaltyKartica;
	}

	public boolean isLoyaltyKarticaNaCekanju() {
		return isLoyaltyKarticaNaCekanju;
	}

	public void setLoyaltyKarticaNaCekanju(boolean isLoyaltyKarticaNaCekanju) {
		this.isLoyaltyKarticaNaCekanju = isLoyaltyKarticaNaCekanju;
	}

	public Set<KupljenaKnjiga> getKupljenaKnjiga() {
		return kupljenaKnjiga;
	}

	public void setKupljenaKnjiga(Set<KupljenaKnjiga> kupljenaKnjiga) {
		this.kupljenaKnjiga = kupljenaKnjiga;
	}

	public boolean isKupljenaKnjiga() {
		return isKupljenaKnjiga;
	}

	public void setKupljenaKnjiga(boolean isKupljenaKnjiga) {
		this.isKupljenaKnjiga = isKupljenaKnjiga;
	}

    
}
