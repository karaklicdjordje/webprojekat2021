package web.knjizara.korisnik;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import web.knjizara.autentifikacija.KorisnikDetails;
import web.knjizara.dao.KorisnikDAO;
import web.knjizara.loyaltyKartica.LoyaltyKartica;
import web.knjizara.loyaltyKartica.LoyaltyKarticaRepository;
import web.knjizara.loyaltyKartica.StatusKartice;

@Repository
public class KorisnikRepository implements KorisnikDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	LoyaltyKarticaRepository loyaltyKarticaRepository;

	private class UserRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long id = rs.getLong("id");
			String korisnickoIme = rs.getString("korisnicko_ime");
			String lozinka = rs.getString("lozinka");
			String email = rs.getString("email");
			String ime = rs.getString("ime");
			String prezime = rs.getString("prezime");
			String adresa = rs.getString("adresa");
			String brojTelefona = rs.getString("telefon");
			Uloga uloga = Uloga.vratiUloguByInt(rs.getInt("uloga"));
			Timestamp vremeRegistracijeSql = rs.getTimestamp("vreme_registracije");
			LocalDateTime vremeRegistracije = vremeRegistracijeSql.toLocalDateTime();
			Date datumRodjenja = rs.getDate("datum_rodjenja");
			Korisnik korisnik = new Korisnik();
			korisnik.setId(id);
			korisnik.setKorisnickoIme(korisnickoIme);
			korisnik.setLozinka(lozinka);
			korisnik.setEmail(email);
			korisnik.setIme(ime);
			korisnik.setPrezime(prezime);
			korisnik.setAdresa(adresa);
			korisnik.setBrojTelefona(brojTelefona);
			korisnik.setUloga(uloga);
			korisnik.setVremeRegistracije(vremeRegistracije);
			korisnik.setDatumRodjenja(datumRodjenja);
			korisnik.setHasLoyaltyKartica(hasLoyaltyKartica(id));
			korisnik.setLoyaltyKarticaNaCekanju(isLoyaltyKarticaNaCekanju(id));
			return korisnik;
		}

	}

	@Override
	public Optional<Korisnik> findById(Long id) {
		String sql = "SELECT * FROM korisnik WHERE id=?";
		Korisnik korisnik = jdbcTemplate.query(sql, new UserRowMapper(), id).get(0);

		return Optional.ofNullable(korisnik);
	}

	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT * FROM korisnik";
		return jdbcTemplate.query(sql, new UserRowMapper());
	}

	@Override
	public Korisnik findByKorisnickoIme(String korisnickoIme) {
		String sql = "SELECT * FROM korisnik WHERE korisnicko_ime=?";
		Korisnik korisnik = jdbcTemplate.query(sql, new UserRowMapper(), korisnickoIme).get(0);
		return korisnik;
	}

	@Transactional
	@Override
	public void save(Korisnik korisnik) {

		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO korisnik (korisnicko_ime, lozinka, email, ime, prezime, "
						+ "adresa, telefon, uloga, vreme_registracije, datum_rodjenja)"
						+ "VALUES (?,?,?,?,?,?,?,?,?,?)";
				int index = 1;
				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				preparedStatement.setString(index++, korisnik.getKorisnickoIme());
				preparedStatement.setString(index++, korisnik.getLozinka());
				preparedStatement.setString(index++, korisnik.getEmail());
				preparedStatement.setString(index++, korisnik.getIme());
				preparedStatement.setString(index++, korisnik.getPrezime());
				preparedStatement.setString(index++, korisnik.getAdresa());
				preparedStatement.setString(index++, korisnik.getBrojTelefona());
				preparedStatement.setInt(index++, korisnik.getUloga().ordinal());
				Timestamp timestampVremeRegistracije = Timestamp.valueOf(korisnik.getVremeRegistracije());
				preparedStatement.setTimestamp(index++, timestampVremeRegistracije);
				preparedStatement.setDate(index++, korisnik.getDatumRodjenja());

				return preparedStatement;
			}
		};

		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(preparedStatementCreator, keyHolder);

	}

	@Override
	public void update(Korisnik korisnik) {
		
		if(korisnik.getUloga()==null) {
			korisnik.setUloga(Uloga.KUPAC);
		}

		String sql = "UPDATE korisnik SET email = ?, ime=?, prezime=?, "
				+ "adresa=?, telefon=?, datum_rodjenja=?, uloga=? WHERE id=?";

		jdbcTemplate.update(sql, korisnik.getEmail(), korisnik.getIme(), korisnik.getPrezime(), korisnik.getAdresa(),
				korisnik.getBrojTelefona(), korisnik.getDatumRodjenja(), korisnik.getUloga().ordinal(), korisnik.getId());
	}

	@Override
	public void delete(Korisnik korisnik) {
		String sql = "DELETE FROM korisnik WHERE id=?";
		jdbcTemplate.update(sql, korisnik.getId());
	}

	@Override
	public Korisnik findKorisnikByKorisnikDetails() {
		if(getKorisnikIdByKorisnikDetails()!=null) {
			return findById(getKorisnikIdByKorisnikDetails()).get();
		}
		return new Korisnik();
	}

	@Override
	public Long getKorisnikIdByKorisnikDetails() {
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			KorisnikDetails korisnikDetails = (KorisnikDetails) authentication.getPrincipal();
			return korisnikDetails.getKorisnikId();
		} catch (Exception e) {
			e.getStackTrace();
		}
		return null;
	}

	@Override
	public boolean hasLoyaltyKartica(Long korisnikId) {
		if(korisnikId != null){
			LoyaltyKartica kartica = loyaltyKarticaRepository.findByKorisnikId(korisnikId);
			if (kartica != null) {
				if (kartica.getStatusKartice().equals(StatusKartice.ODOBREN)) {
					return true;
				}
			}
		} 
		return false;
	}

	@Override
	public boolean isLoyaltyKarticaNaCekanju(Long korisnikId) {
		if(korisnikId != null) {
			LoyaltyKartica kartica = loyaltyKarticaRepository.findByKorisnikId(korisnikId);
			if (kartica != null) {
				if (kartica.getStatusKartice().equals(StatusKartice.NA_CEKANJU)) {
					return true;
				}
			}
		} 
		return false;
	}

}
