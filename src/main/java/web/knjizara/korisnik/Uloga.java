package web.knjizara.korisnik;

import org.springframework.security.core.GrantedAuthority;

public enum Uloga implements GrantedAuthority {
	
	KUPAC, ADMINISTRATOR, BLOKIRAN;

	@Override
	public String getAuthority() {
		return this.name();
	}
	
	public static Uloga vratiUloguByInt(int broj) {
		switch (broj) {
		case 0:
			 return Uloga.KUPAC;
		case 1:
			return Uloga.ADMINISTRATOR;
		case 2: 
			return Uloga.BLOKIRAN;
		default:
			return null;
		}
	}
	
	
}
