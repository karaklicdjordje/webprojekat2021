package web.knjizara.korisnik;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import web.knjizara.loyaltyKartica.LoyaltyKartica;
import web.knjizara.loyaltyKartica.LoyaltyKarticaRepository;
import web.knjizara.loyaltyKartica.StatusKartice;

@Controller
public class KorisnikController {

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Autowired
	private LoyaltyKarticaRepository loyaltyKarticaRepository;

	@GetMapping("")
	public String prikaziPocetnuStranu() {
		return "redirect:/knjige";
	}

	@GetMapping("/registracija")
	public String prikaziLoginFormu(Model model) {
		model.addAttribute("korisnik", new Korisnik());

		return "registracija_form";
	}

	@PostMapping("/proces_registracije")
	public String procesRegistracije(Korisnik korisnik, boolean loyaltyKartica) {
		LocalDateTime vremeRegistracije = LocalDateTime.now();
		korisnik.setVremeRegistracije(vremeRegistracije);
		korisnik.setUloga(Uloga.KUPAC);

		// Enkripcija passworda
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encodedPassword = encoder.encode(korisnik.getLozinka());
		//
		korisnik.setLozinka(encodedPassword);

		korisnikRepository.save(korisnik);

		korisnik = korisnikRepository.findByKorisnickoIme(korisnik.getKorisnickoIme());

		posaljiZahtevZaLoyaltyKarticu(loyaltyKartica, korisnik);

		return "uspesna_registracija";
	}

	@GetMapping("/korisnici")
	public String prikaziListuKorisnika(Model model) {
		List<Korisnik> listaKorisnika = korisnikRepository.findAll();
		model.addAttribute("listaKorisnika", listaKorisnika);
		return "korisnici";
	}

	@GetMapping("/korisnici/profil")
	public String prikaziProfilKorisnika(Model model) {
		Korisnik korisnik = korisnikRepository.findKorisnikByKorisnikDetails();
		model.addAttribute("korisnik", korisnik);
		return "korisnik_profil";
	}

	@GetMapping("/korisnici/profil/azuriraj")
	public String prikaziProfilKorisnikaZaAzuriranje(Model model) {
		Korisnik korisnik = korisnikRepository.findKorisnikByKorisnikDetails();
		model.addAttribute("korisnik", korisnik);
		return "korisnik_profil_azuriranje";
	}

	@PutMapping("/korisnici/profil/azuriraj")
	public String azurirajProfilKorisnika(Korisnik korisnik, boolean loyaltyKartica) {
		korisnikRepository.update(korisnik);
		posaljiZahtevZaLoyaltyKarticu(loyaltyKartica, korisnik);
		return "redirect:/korisnici/profil";
	}
	
	@GetMapping("/admin/korisnici/profil/{korisnikId}")
	public String prikaziProfilKorisnikaZaAzuriranjeAdmin(Model model, @PathVariable Long korisnikId) {
		Korisnik korisnik = korisnikRepository.findById(korisnikId).get();
		model.addAttribute("korisnik", korisnik);
		return "korisnik_profil_azuriranje_admin";
	}

	@PutMapping("/admin/korisnici/profil/azuriraj")
	public String azurirajProfilKorisnikaAdmin(Model model, Korisnik korisnik) {
		Korisnik korisnikIzBaze = korisnikRepository.findById(korisnik.getId()).get();
		if(korisnikIzBaze.getUloga().equals(Uloga.ADMINISTRATOR)) {
			if(!korisnik.getUloga().equals(Uloga.ADMINISTRATOR)) {
				String errorMessage = "Ne mozete promeniti ulogu administratora";
				model.addAttribute("errorMessage", errorMessage);
				return prikaziProfilKorisnikaZaAzuriranjeAdmin(model, korisnikIzBaze.getId());
			}
		}
		korisnikRepository.update(korisnik);
		return "redirect:/korisnici";
	}

	public void posaljiZahtevZaLoyaltyKarticu(boolean loyaltyKartica, Korisnik korisnik) {
		if (!loyaltyKartica) {
			return;
		}
		LoyaltyKartica kartica = loyaltyKarticaRepository.findByKorisnikId(korisnik.getId());
		if (kartica != null) {
			kartica.setStatusKartice(StatusKartice.NA_CEKANJU);
			loyaltyKarticaRepository.update(kartica);
			korisnik.setLoyaltyKarticaNaCekanju(true);
			return;
		}
		kartica = new LoyaltyKartica();
		kartica.setKorisnikId(korisnik.getId());
		kartica.setBrojPoena(0);
		kartica.setStatusKartice(StatusKartice.NA_CEKANJU);
		loyaltyKarticaRepository.save(kartica);
		korisnik.setLoyaltyKarticaNaCekanju(true);
	}

}
