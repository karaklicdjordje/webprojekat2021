package web.knjizara.autentifikacija;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;

public class KorisnikDetailsService implements UserDetailsService {
	
	@Autowired
	private KorisnikRepository korisnikRepository;

	@Override
	public UserDetails loadUserByUsername(String korisnickoIme) throws UsernameNotFoundException {
		Korisnik korisnik = korisnikRepository.findByKorisnickoIme(korisnickoIme);
		if(korisnik == null) {
			throw new UsernameNotFoundException("Korisnik nije pronadjen");
		}
		
		return new KorisnikDetails(korisnik);
	}
	
}
