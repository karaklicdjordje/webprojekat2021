package web.knjizara.autentifikacija;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;
import web.knjizara.korisnik.Uloga;

public class KorisnikDetails implements UserDetails {
	
	@Autowired
	KorisnikRepository korisnikRepository;

	private Korisnik korisnik;

	public KorisnikDetails(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<Uloga> authorities = new ArrayList<>();	
		authorities.add(korisnik.getUloga());
		
		return authorities;
	}

	@Override
	public String getPassword() {
		return korisnik.getLozinka();
	}

	@Override
	public String getUsername() {
		return korisnik.getKorisnickoIme();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() throws LockedException{
		if(korisnik.getUloga().equals(Uloga.BLOKIRAN)) {
			throw new LockedException("Korisnik je blokiran");
		}
		return true;
	}

	public String getImeIPrezime() {
		return korisnik.getIme() + " " + korisnik.getPrezime();
	}
	
	public String getUloga() {
		return korisnik.getUloga().toString();
	}
	
	public boolean isAdmin() {
		if (korisnik.getUloga().equals(Uloga.ADMINISTRATOR)) {
			return true;
		} 
		return false;
	}
	
	public boolean isKupac() {
		if (korisnik.getUloga().equals(Uloga.KUPAC)) {
			return true;
		} 
		return false;
	}
	
	public Long getKorisnikId() {
		return korisnik.getId();
	}
	
	public boolean hasLoyaltyKartica() {
		return korisnik.isHasLoyaltyKartica();
	}
	
	public boolean isLoyaltyKarticaNaCekanju() {
		return korisnik.isLoyaltyKarticaNaCekanju();
	}

}
