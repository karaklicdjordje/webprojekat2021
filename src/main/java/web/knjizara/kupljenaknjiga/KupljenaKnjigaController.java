package web.knjizara.kupljenaknjiga;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import web.knjizara.knjiga.Knjiga;
import web.knjizara.knjiga.KnjigaRepository;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;
import web.knjizara.korpa.Korpa;
import web.knjizara.korpa.KorpaController;
import web.knjizara.korpa.KorpaRepository;
import web.knjizara.kupovina.Kupovina;
import web.knjizara.kupovina.KupovinaRepository;
import web.knjizara.loyaltyKartica.LoyaltyKartica;
import web.knjizara.loyaltyKartica.LoyaltyKarticaRepository;
import web.knjizara.loyaltyKartica.StatusKartice;

@Controller
public class KupljenaKnjigaController {

	@Autowired
	private KupljenaKnjigaRepository kupljenaKnjigaRepository;
	
	@Autowired
	private KorpaRepository korpaRepository;
	
	@Autowired
	private KorpaController korpaController;

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;
	
	@Autowired
	private KupovinaRepository kupovinaRepository;
	
	@Autowired
	private LoyaltyKarticaRepository loyaltyKarticaRepository;
	
	@PostMapping("/kupljenaKnjiga")
	public String kupiKnjige(Model model, Long korisnik_id, Integer brojPoena) {
		Kupovina kupovina = new Kupovina();
		int brojKupljenihKnjiga = 0;
		double ukupnaCenaKupovine = 0.0;
		Korisnik korisnik = korisnikRepository.findById(korisnik_id).get();
		List<Korpa> listKorpa = korpaRepository.findAllByKorisnikId(korisnik_id);
		for (Korpa korpa : listKorpa) {
			brojKupljenihKnjiga++;
			ukupnaCenaKupovine+= korpa.getCena();
			}
			kupovina.setVremeKupovine(LocalDateTime.now());
			kupovina.setBrojKupljenihKnjiga(brojKupljenihKnjiga);
			kupovina.setKorisnik(korisnik);

			double popust = 0;
			if (brojPoena!=null && brojPoena>0) {
				popust = ukupnaCenaKupovine * (brojPoena*5) / 100;
			}
			
			kupovina.setUkupnaCenaKupovina(ukupnaCenaKupovine-popust);
			
			kupovinaRepository.save(kupovina);

			Kupovina kupovinadb = kupovinaRepository.findAll().get(kupovinaRepository.findAll().size()-1);
		for (Korpa korpa : listKorpa) {
			KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga();
			kupljenaKnjiga.setKorisnik(korpa.getKorisnik());
			kupljenaKnjiga.setKnjiga(korpa.getKnjiga());
			
			double popustPoKorpi=0;
			if(brojPoena!=null && brojPoena>0) {
				popustPoKorpi = korpa.getCena() * (brojPoena*5) / 100;
			}
			
			kupljenaKnjiga.setCena(korpa.getCena()-popustPoKorpi);
			kupljenaKnjiga.setBrojPrimeraka(korpa.getBrojPrimeraka());
			kupljenaKnjiga.setKupovina(kupovinadb);
			kupljenaKnjigaRepository.save(kupljenaKnjiga);
			Knjiga knjiga = knjigaRepository.findById(korpa.getKnjiga().getIsbn()).get();
			int preostaliBrojPrimeraka = knjiga.getBrojPrimeraka()- korpa.getBrojPrimeraka();
			knjiga.setBrojPrimeraka(preostaliBrojPrimeraka);
			knjigaRepository.update(knjiga);
			korpaRepository.delete(korpa);			
		}
		
		String successMessage = "Cestitamo na kupovini! ";
		LoyaltyKartica loyaltyKartica = loyaltyKarticaRepository.findByKorisnikId(korisnik_id);
		Integer ostvareniPoeniOdKupovine = (int) (ukupnaCenaKupovine/1000);
		if(loyaltyKartica != null) {
			if(loyaltyKartica.getStatusKartice().equals(StatusKartice.ODOBREN)) {
				successMessage += "Ovom kupovinom ste zaradili " + ostvareniPoeniOdKupovine + " poena na vasoj Loyalty kartici.";
			}
		}

		if(brojPoena != null && brojPoena>0) {
			
			loyaltyKartica.setBrojPoena(loyaltyKartica.getBrojPoena() - brojPoena +ostvareniPoeniOdKupovine);
			
			loyaltyKarticaRepository.update(loyaltyKartica);
			
			successMessage += " Koriscenjem vase Loyalty kartice ostvarili ste popust od " + popust + " dinara.";
					
		}

		model.addAttribute("successMessage", successMessage);
			
		return korpaController.prikaziListuKnjiga(model);
	}
}
