package web.knjizara.kupljenaknjiga;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import web.knjizara.dao.KupljenaKnjigaDAO;
import web.knjizara.knjiga.Knjiga;
import web.knjizara.knjiga.KnjigaRepository;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;
import web.knjizara.kupovina.Kupovina;
import web.knjizara.kupovina.KupovinaRepository;

@Repository
public class KupljenaKnjigaRepository implements KupljenaKnjigaDAO {

	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;
	
	@Autowired
	private KupovinaRepository kupovinaRepository;

	private class KupljenaKnjigaRowMapper implements RowMapper<KupljenaKnjiga> {

		private Map<Long, KupljenaKnjiga> kupljeneKnjigeMap = new LinkedHashMap<>();

		@Override
		public KupljenaKnjiga mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long kupljenaKnjigaId = rs.getLong("kupljena_knjiga_id");
			String knjigaId = rs.getString("knjiga_id");
			Long kupovinaId = rs.getLong("kupovina_id");
			Integer ukupanBrojPrimeraka = rs.getInt("broj_primeraka_knjige");
			Double ukupnaCenaKnjige = rs.getDouble("ukupna_cena_knjige");
			Knjiga knjiga = knjigaRepository.findById(knjigaId).get();
			Long korisnikId = rs.getLong("korisnik_id");
			Korisnik korisnik = korisnikRepository.findById(korisnikId).get();
			Kupovina kupovina = kupovinaRepository.findById(kupovinaId).get();
			Double cena = rs.getDouble("cena");
			Integer brojPrimeraka = rs.getInt("broj_primeraka");
			KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga();
			kupljenaKnjiga.setKupljenaKnjigaId(kupljenaKnjigaId);
			kupljenaKnjiga.setKnjiga(knjiga);
			kupljenaKnjiga.setKorisnik(korisnik);
			kupljenaKnjiga.setCena(cena);
			kupljenaKnjiga.setBrojPrimeraka(brojPrimeraka);
			kupljenaKnjiga.setKupovina(kupovina);
			kupljenaKnjiga.setUkupanBrojKnjiga(ukupanBrojPrimeraka);
			kupljenaKnjiga.setUkupnaCena(ukupnaCenaKnjige);
			kupljeneKnjigeMap.put(kupljenaKnjigaId, kupljenaKnjiga);
			return kupljenaKnjiga;
		}

		public List<KupljenaKnjiga> vratiKupljeneKnjige() {
			return new ArrayList<>(kupljeneKnjigeMap.values());
		}

	}
	
	@Override
	public Optional<KupljenaKnjiga> findById(String id) {
		String sql = "SELECT k.*, kg.naziv, kg.autor,  SUM(k.broj_primeraka) AS broj_primeraka_knjige,  SUM(k.cena) AS ukupna_cena_knjige  FROM kupovina kup LEFT JOIN kupljena_knjiga k ON kup.kupovina_id = k.kupovina_id LEFT JOIN knjiga kg ON k.knjiga_id = kg.isbn where kup.kupovina_id=?";
		
		KupljenaKnjigaRowMapper rowCallbackHandler = new KupljenaKnjigaRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		KupljenaKnjiga kupljenaKnjiga = rowCallbackHandler.vratiKupljeneKnjige().get(0);

		return Optional.ofNullable(kupljenaKnjiga);
	}
	
	
	@Override
	public List<KupljenaKnjiga> findAllByKupovinaId(Long kupovinaId) {
		String sql = "SELECT k.*, kg.naziv, kg.autor,  SUM(k.broj_primeraka) AS broj_primeraka_knjige,  SUM(k.cena) AS ukupna_cena_knjige  FROM kupovina kup LEFT JOIN kupljena_knjiga k ON kup.kupovina_id = k.kupovina_id LEFT JOIN knjiga kg ON k.knjiga_id = kg.isbn where kup.kupovina_id=?";
		
		KupljenaKnjigaRowMapper rowCallbackHandler = new KupljenaKnjigaRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler, kupovinaId);

		return rowCallbackHandler.vratiKupljeneKnjige();
	}

	@Override
	public List<KupljenaKnjiga> findAll() {
		String sql = "SELECT k.*, kg.naziv, kg.autor,  SUM(k.broj_primeraka) AS broj_primeraka_knjige,  SUM(k.cena) AS ukupna_cena_knjige "
				+ " FROM kupljena_knjiga k LEFT JOIN kupovina kup ON k.kupovina_id = kup.kupovina_id LEFT JOIN knjiga kg ON k.knjiga_id = kg.isbn " +
				" GROUP BY k.knjiga_id";

KupljenaKnjigaRowMapper rowCallbackHandler = new KupljenaKnjigaRowMapper();
jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.vratiKupljeneKnjige();
	}
	
	@Override
	public List<KupljenaKnjiga> findAllByVremeKupovine(LocalDateTime startDate, LocalDateTime endDate) {
		String sql = "SELECT k.*, kg.naziv, kg.autor,  SUM(k.broj_primeraka) AS broj_primeraka_knjige,  SUM(k.cena) AS ukupna_cena_knjige  FROM kupovina kup  JOIN kupljena_knjiga k ON kup.kupovina_id = k.kupovina_id LEFT JOIN knjiga kg ON k.knjiga_id = kg.isbn where vreme_kupovine BETWEEN ? AND ? group by k.knjiga_id";

KupljenaKnjigaRowMapper rowCallbackHandler = new KupljenaKnjigaRowMapper();
jdbcTemplate.query(sql, rowCallbackHandler, startDate, endDate);
		return rowCallbackHandler.vratiKupljeneKnjige();
	}

	@Override
	public void save(KupljenaKnjiga kupljenaKnjiga) {
		String sql = "INSERT INTO kupljena_knjiga(knjiga_id, korisnik_id, cena, broj_primeraka, kupovina_id) " + "VALUES('"
				+ kupljenaKnjiga.getKnjiga().getIsbn() + "','" + kupljenaKnjiga.getKorisnik().getId() + "','" + kupljenaKnjiga.getCena() + "','"
				+ kupljenaKnjiga.getBrojPrimeraka() + "','" + kupljenaKnjiga.getKupovina().getKupovinaId() + "')";
		jdbcTemplate.update(sql);
	}

	@Override
	public void update(KupljenaKnjiga kupljenaKnjiga) {
		
		
	}

	@Override
	public void delete(KupljenaKnjiga t) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<KupljenaKnjiga> findAllByKupovinaId(Kupovina kupovinaId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<KupljenaKnjiga> findAllByKupovinaVremeKupovine(LocalDateTime dateTime) {
		dateTime = dateTime.minusHours(1);
		String dateTimeString = dateTime.toString();
		dateTimeString = dateTimeString.replace("T", " ");
		String sql = "SELECT k.*, kg.naziv, kg.autor,  SUM(k.broj_primeraka) AS broj_primeraka_knjige,  SUM(k.cena) AS ukupna_cena_knjige  FROM kupovina kup  JOIN kupljena_knjiga k ON kup.kupovina_id = k.kupovina_id LEFT JOIN knjiga kg ON k.knjiga_id = kg.isbn where kup.vreme_kupovine like '" + dateTimeString + "%' group by k.knjiga_id";

		KupljenaKnjigaRowMapper rowCallbackHandler = new KupljenaKnjigaRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler);
		return rowCallbackHandler.vratiKupljeneKnjige();
	}

}
