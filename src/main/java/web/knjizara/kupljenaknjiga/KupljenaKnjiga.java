package web.knjizara.kupljenaknjiga;

import web.knjizara.knjiga.Knjiga;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.kupovina.Kupovina;


public class KupljenaKnjiga {

	private Long kupljenaKnjigaId;
	
	private Integer brojPrimeraka;
	
	private Double cena;

	private Knjiga knjiga;
	
	private Korisnik korisnik;
	
	private Kupovina kupovina;
	
	private double ukupnaCena;
	private int ukupanBrojKnjiga;

	
	
	public KupljenaKnjiga(Long kupljenaKnjigaId, Integer brojPrimeraka, Double cena, Knjiga knjiga, Korisnik korisnik,
			Kupovina kupovina) {
		super();
		this.kupljenaKnjigaId = kupljenaKnjigaId;
		this.brojPrimeraka = brojPrimeraka;
		this.cena = cena;
		this.knjiga = knjiga;
		this.korisnik = korisnik;
		this.kupovina = kupovina;
	}
	
	

	public double getUkupnaCena() {
		return ukupnaCena;
	}



	public void setUkupnaCena(double ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}



	public int getUkupanBrojKnjiga() {
		return ukupanBrojKnjiga;
	}



	public void setUkupanBrojKnjiga(int ukupanBrojKnjiga) {
		this.ukupanBrojKnjiga = ukupanBrojKnjiga;
	}



	public Kupovina getKupovina() {
		return kupovina;
	}

	public void setKupovina(Kupovina kupovina) {
		this.kupovina = kupovina;
	}

	public KupljenaKnjiga() {
		
	}

	public Long getKupljenaKnjigaId() {
		return kupljenaKnjigaId;
	}

	public void setKupljenaKnjigaId(Long kupljenaKnjigaId) {
		this.kupljenaKnjigaId = kupljenaKnjigaId;
	}

	public Integer getBrojPrimeraka() {
		return brojPrimeraka;
	}

	public void setBrojPrimeraka(Integer brojPrimeraka) {
		this.brojPrimeraka = brojPrimeraka;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	
}

