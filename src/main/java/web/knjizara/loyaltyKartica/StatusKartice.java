package web.knjizara.loyaltyKartica;

public enum StatusKartice {
	
	NA_CEKANJU, ODOBREN, ODBIJEN;
	
	public static StatusKartice vratiStatusKarticeByInt(int broj) {
		switch (broj) {
		case 0:
			 return StatusKartice.NA_CEKANJU;
		case 1:
			return StatusKartice.ODOBREN;
		case 2:
			return StatusKartice.ODBIJEN;
		default:
			return null;
		}
	}

}
