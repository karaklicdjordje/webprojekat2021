package web.knjizara.loyaltyKartica;

import web.knjizara.korisnik.Korisnik;


public class LoyaltyKartica {
	
	private Long id;
	
	private Integer brojPoena;
	
	private Long korisnikId;
	
	private StatusKartice statusKartice;

	
	public LoyaltyKartica(Long id, Integer brojPoena, Long korisnikId, StatusKartice statusKartice) {
		super();
		this.id = id;
		this.brojPoena = brojPoena;
		this.korisnikId = korisnikId;
		this.statusKartice = statusKartice;
	}

	public LoyaltyKartica() {
		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBrojPoena() {
		return brojPoena;
	}

	public void setBrojPoena(Integer brojPoena) {
		this.brojPoena = brojPoena;
	}

	public Long getKorisnikId() {
		return korisnikId;
	}

	public void setKorisnikId(Long korisnikId) {
		this.korisnikId = korisnikId;
	}

	public StatusKartice getStatusKartice() {
		return statusKartice;
	}

	public void setStatusKartice(StatusKartice statusKartice) {
		this.statusKartice = statusKartice;
	}
	
}

