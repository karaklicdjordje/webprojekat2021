package web.knjizara.loyaltyKartica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import web.knjizara.dao.LoyaltyKarticaDAO;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;

@Repository
public class LoyaltyKarticaRepository implements LoyaltyKarticaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class UserRowMapper implements RowMapper<LoyaltyKartica> {

		@Override
		public LoyaltyKartica mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long id = rs.getLong("id");
			Long korisnikId = rs.getLong("korisnik_id");
			Integer brojPoena = rs.getInt("broj_poena");
			StatusKartice statusKartice = StatusKartice.vratiStatusKarticeByInt(rs.getInt("status_kartice"));
			LoyaltyKartica loyaltyKartica = new LoyaltyKartica();
			loyaltyKartica.setId(id);
			loyaltyKartica.setKorisnikId(korisnikId);
			loyaltyKartica.setBrojPoena(brojPoena);
			loyaltyKartica.setStatusKartice(statusKartice);
			return loyaltyKartica;
		}

	}

	@Override
	public Optional<LoyaltyKartica> findById(Long id) {
		try {
			String sql = "SELECT * FROM loyalty_kartica WHERE id=?";
			LoyaltyKartica loyaltyKartica = jdbcTemplate.query(sql, new UserRowMapper(), id).get(0);
			return Optional.ofNullable(loyaltyKartica);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<LoyaltyKartica> findAll() {
		String sql = "SELECT * FROM loyalty_kartica";
		return jdbcTemplate.query(sql, new UserRowMapper());
	}

	@Override
	public LoyaltyKartica findByKorisnikId(Long korisnikId) {
		try {
			String sql = "SELECT * FROM loyalty_kartica WHERE korisnik_id=?";
			return jdbcTemplate.query(sql, new UserRowMapper(), korisnikId).get(0);
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public void save(LoyaltyKartica loyaltyKartica) {
		String sql = "INSERT INTO loyalty_kartica(korisnik_id, broj_poena, status_kartice) " + "VALUES('"
				+ loyaltyKartica.getKorisnikId() + "','" + loyaltyKartica.getBrojPoena() + "','"
				+ loyaltyKartica.getStatusKartice().ordinal() + "')";

		jdbcTemplate.update(sql);
	}

	@Override
	public void update(LoyaltyKartica loyaltyKartica) {
		String sql = "UPDATE loyalty_kartica SET broj_poena =?, status_kartice=? WHERE id=?";
		jdbcTemplate.update(sql, loyaltyKartica.getBrojPoena(),
				loyaltyKartica.getStatusKartice().ordinal(), loyaltyKartica.getId());
	}

	@Override
	public void delete(LoyaltyKartica loyaltyKartica) {
		String sql = "DELETE FROM loyalty_kartica WHERE id=?";
		jdbcTemplate.update(sql, loyaltyKartica.getId());
	}

	@Override
	public List<LoyaltyKartica> findAllByStatusKartice(StatusKartice statusKartice) {
		String sql = "SELECT * FROM loyalty_kartica WHERE status_kartice=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), statusKartice);
	}

	public void azurirajPoene(LoyaltyKartica loyaltyKartica) {
		String sql = "UPDATE loyalty_kartica SET broj_poena =?, WHERE id=?";
		jdbcTemplate.update(sql, loyaltyKartica.getBrojPoena(), loyaltyKartica.getId());
	}

	public double iskoristiPopust(int brojPoena, double cena) {
		if (brojPoena > 0 && brojPoena <= 10) {
			double popust = brojPoena * 5;
			cena = cena - (cena * 100 / popust);
		}
		return cena;
	}

}
