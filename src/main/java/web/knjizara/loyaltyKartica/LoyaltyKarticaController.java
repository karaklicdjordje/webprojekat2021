package web.knjizara.loyaltyKartica;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;

@Controller
public class LoyaltyKarticaController {

	@Autowired
	private LoyaltyKarticaRepository loyaltyKarticaRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;

	@GetMapping("/admin/loyalty")
	public String prikaziSveKartice(Model model) {
		List<LoyaltyKartica> listaKartica = loyaltyKarticaRepository.findAll();
		Map<Korisnik, LoyaltyKartica> mapaKartica = new HashMap<>();

		for (LoyaltyKartica loyaltyKartica : listaKartica) {
			Korisnik korisnik = korisnikRepository.findById(loyaltyKartica.getKorisnikId()).get();
			mapaKartica.put(korisnik, loyaltyKartica);
		}
		model.addAttribute("mapaKartica", mapaKartica);
		return "loyaltyKartice";
	}

	@GetMapping("/admin/loyaltyStatus")
	public void azurirajLoyaltyKarticu(@RequestParam(name = "loyaltyId") Long id, @RequestParam(name = "statusKartice") String statusKartice) {
		LoyaltyKartica loyaltyKartica = loyaltyKarticaRepository.findById(id).get();
		int statusKarticeParse = Integer.parseInt(statusKartice);
		switch (statusKarticeParse) {
		case 1:
			loyaltyKartica.setStatusKartice(StatusKartice.ODOBREN);
			loyaltyKartica.setBrojPoena(4);
			break;
		case 2:
			loyaltyKartica.setStatusKartice(StatusKartice.ODBIJEN);
		default:
			break;
		};
		loyaltyKarticaRepository.update(loyaltyKartica);
		
	}

}
