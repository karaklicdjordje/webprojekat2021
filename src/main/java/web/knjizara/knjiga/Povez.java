package web.knjizara.knjiga;

public enum Povez {
	
	TVRDI, MEKI;

	public static Povez vratiPovezByInt(int broj) {
		switch (broj) {
		case 0:
			 return Povez.TVRDI;
		case 1:
			return Povez.MEKI;
		default:
			return null;
		}
	}

}
