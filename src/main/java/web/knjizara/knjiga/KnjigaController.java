package web.knjizara.knjiga;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.multipart.MultipartFile;

import web.knjizara.korpa.Korpa;
import web.knjizara.zanr.Zanr;
import web.knjizara.zanr.ZanrRepository;

@Controller
public class KnjigaController {

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private ZanrRepository zanrRepository;

	@GetMapping("/knjige")
	public String prikaziListuKnjiga(Model model) {
		List<Knjiga> listaKnjiga = knjigaRepository.findAll();
		model.addAttribute("listaKnjiga", listaKnjiga);
		return "knjige";
	}

	@GetMapping("/knjiga/{isbn}")
	public String prikaziKnjigu(Model model, @PathVariable String isbn) {
		Knjiga knjiga = knjigaRepository.findById(isbn).get();
		model.addAttribute("knjiga", knjiga);
		try {
			model.addAttribute("slika", Base64.getEncoder().encodeToString(knjiga.getSlika()));
		} catch (Exception e) {
			
		}
		Korpa korpa = new Korpa();
		model.addAttribute("korpa", korpa);
		return "knjiga_detalji";
	}

	@GetMapping("/knjige/dodavanje")
	public String dodajKnjigu(Model model) {
		List<Zanr> listaZanrova = zanrRepository.findAll();
		model.addAttribute("knjiga", new Knjiga());
		model.addAttribute("listaZanrova", listaZanrova);
		return "knjiga_dodavanje";
	}

	@PostMapping("/knjige")
	public String dodajKnjigu(Knjiga knjiga, Long[] zanrIds, MultipartFile file) {
		double prosecnaOcena = (Math.random() * 4 + 1);
		prosecnaOcena = Math.round(prosecnaOcena * 100.0) / 100.0;
		knjiga.setProsecnaOcenaKnjige(prosecnaOcena);
		try {
			knjiga.setSlika(file.getBytes());
		} catch (IOException e) {
			
		}
		for (Long zanrId : zanrIds) {
			Zanr zanr = zanrRepository.findById(zanrId).get();
			knjiga.getZanrovi().add(zanr);
		}
		knjigaRepository.save(knjiga);
		return "uspesno_dodavanje_knjige";
	}

	@GetMapping("/knjiga/azuriraj/{isbn}")
	public String prikaziKnjiguZaAzuriranje(Model model, @PathVariable String isbn) {
		List<Zanr> listaZanrova = zanrRepository.findAll();
		Knjiga knjiga = knjigaRepository.findById(isbn).get();

		Map<Zanr, Boolean> mapaZanrova = vratiMapuZanrova(listaZanrova, knjiga.getZanrovi());
		try {
			model.addAttribute("slika", Base64.getEncoder().encodeToString(knjiga.getSlika()));
		} catch (Exception e) {
			
		}
		model.addAttribute("knjiga", knjiga);
		model.addAttribute("mapaZanrova", mapaZanrova);
		return "knjiga_azuriranje";
	}

	@PutMapping("/knjige/azuriraj")
	public String azurirajKnjigu(Knjiga knjiga, Long[] zanrIds, MultipartFile file) {
		for (Long zanrId : zanrIds) {
			Zanr zanr = zanrRepository.findById(zanrId).get();
			knjiga.getZanrovi().add(zanr);
		}
		try {
			knjiga.setSlika(file.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		knjigaRepository.update(knjiga);
		return "uspesno_azuriranje_knjige";
	}

	public Map<Zanr, Boolean> vratiMapuZanrova(List<Zanr> listaZanrova, Set<Zanr> setZanrova) {
		Map<Zanr, Boolean> mapaZanrova = new HashMap<>();
		for (Zanr zanr : listaZanrova) {
			Boolean checked = false;
			for (Zanr checkZanr : setZanrova) {
				if (checkZanr.getId().equals(zanr.getId())) {
					checked = true;
					break;
				}
			}
			mapaZanrova.put(zanr, checked);
		}
		return mapaZanrova;
	}

}
