package web.knjizara.knjiga;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import web.knjizara.dao.KnjigaDAO;
import web.knjizara.zanr.Zanr;

@Repository
public class KnjigaRepository implements KnjigaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KnjigaZanrRowCallBackHandler implements RowCallbackHandler {

		private Map<String, Knjiga> knjigeMapa = new LinkedHashMap<>();

		@Override
		public void processRow(ResultSet rs) throws SQLException {

			String isbn = rs.getString("isbn");
			Knjiga knjiga = knjigeMapa.get(isbn);
			if (knjiga == null) {
				knjiga = new Knjiga();
				knjiga.setIsbn(rs.getString("isbn"));
				knjiga.setAutor(rs.getString("autor"));
				knjiga.setBrojStranica(rs.getInt("broj_stranica"));
				knjiga.setCena(rs.getDouble("cena"));
				knjiga.setGodinaIzdavanja(rs.getInt("godina_izdavanja"));
				knjiga.setIzdavackaKuca(rs.getString("izdavacka_kuca"));
				knjiga.setJezik(rs.getString("jezik"));
				knjiga.setKratakOpis(rs.getString("kratak_opis"));
				knjiga.setNaziv(rs.getString("naziv"));
				byte[] slika = rs.getBytes("slika");
				if(slika!=null) {
					knjiga.setSlikaKnjige(Base64.getEncoder().encodeToString(slika));
				}
				Pismo pismo = Pismo.vratiPismoByInt(rs.getInt("pismo"));
				knjiga.setPismo(pismo);
				Povez povez = Povez.vratiPovezByInt(rs.getInt("povez"));
				knjiga.setPovez(povez);
				knjiga.setProsecnaOcenaKnjige(rs.getDouble("prosecna_ocena_knjige"));
				knjiga.setSlika(rs.getBytes("slika"));
				knjiga.setBrojPrimeraka(rs.getInt("broj_primeraka"));
				knjigeMapa.put(knjiga.getIsbn(), knjiga);
			}

			Long zanrId = rs.getLong("id");
			String imeZanra = rs.getString("ime");
			String opisZanra = rs.getString("opis");
			Zanr zanr = new Zanr();
			zanr.setId(zanrId);
			zanr.setIme(imeZanra);
			zanr.setOpis(opisZanra);

			boolean proveraZanra = true;
			for (Zanr ubacenZanr : knjiga.getZanrovi()) {
				if (zanr.getId().equals(ubacenZanr.getId())) {
					proveraZanra = false;
					break;
				}
			}
			if (proveraZanra) {
				knjiga.getZanrovi().add(zanr);
			}

		}

		public List<Knjiga> vratiSveKnjige() {
			return new ArrayList<>(knjigeMapa.values());
		}

	}

	public Optional<Knjiga> findById(String isbn) {
		String sql = "SELECT k.*, z.* FROM knjiga k " + "LEFT JOIN knjiga_zanr kz ON kz.knjiga_id = k.isbn "
				+ "LEFT JOIN zanr z ON kz.zanr_id = z.id " + " WHERE k.isbn=? " + "ORDER BY k.isbn";

		KnjigaZanrRowCallBackHandler rowCallbackHandler = new KnjigaZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, isbn);

		Knjiga knjiga = rowCallbackHandler.vratiSveKnjige().get(0);

		return Optional.ofNullable(knjiga);
	}

	@Override
	public List<Knjiga> findAll() {
		String sql = "SELECT k.*, z.* FROM knjiga k " + "LEFT JOIN knjiga_zanr kz ON kz.knjiga_id = k.isbn "
				+ "LEFT JOIN zanr z ON kz.zanr_id = z.id " + "ORDER BY k.isbn";

		KnjigaZanrRowCallBackHandler rowCallbackHandler = new KnjigaZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.vratiSveKnjige();
	}

	@Transactional
	@Override
	public void save(Knjiga knjiga) {

		String sql = "INSERT INTO knjiga (isbn, autor, broj_stranica, cena, godina_izdavanja, "
				+ "izdavacka_kuca, jezik, kratak_opis, naziv, pismo, povez, prosecna_ocena_knjige, slika, broj_primeraka)"
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		jdbcTemplate.update(sql, knjiga.getIsbn(), knjiga.getAutor(), knjiga.getBrojStranica(), knjiga.getCena(),
				knjiga.getGodinaIzdavanja(), knjiga.getIzdavackaKuca(), knjiga.getJezik(), knjiga.getKratakOpis(),
				knjiga.getNaziv(), knjiga.getPismo().ordinal(), knjiga.getPovez().ordinal(),
				knjiga.getProsecnaOcenaKnjige(), knjiga.getSlika(), knjiga.getBrojPrimeraka());

		sql = "DELETE from knjiga_zanr WHERE knjiga_id=?";
		jdbcTemplate.update(sql, knjiga.getIsbn());

		for (Zanr zanr : knjiga.getZanrovi()) {
			sql = "INSERT INTO knjiga_zanr(knjiga_id, zanr_id) " + "VALUES('" + knjiga.getIsbn() + "','" + zanr.getId()
					+ "')";
			jdbcTemplate.update(sql);
		}

	}

	@Override
	public void update(Knjiga knjiga) {
		String sql = "UPDATE knjiga SET autor = ?, broj_stranica = ?, cena = ?, godina_izdavanja = ?, "
				+ "izdavacka_kuca = ?, jezik=?, kratak_opis = ?, naziv = ?, pismo=?, povez =?, prosecna_ocena_knjige =?, slika =? , broj_primeraka =? WHERE isbn = ?";
		jdbcTemplate.update(sql, knjiga.getAutor(), knjiga.getBrojStranica(), knjiga.getCena(),
				knjiga.getGodinaIzdavanja(), knjiga.getIzdavackaKuca(), knjiga.getJezik(), knjiga.getKratakOpis(),
				knjiga.getNaziv(), knjiga.getPismo().ordinal(), knjiga.getPovez().ordinal(), knjiga.getProsecnaOcenaKnjige(),
				knjiga.getSlika(), knjiga.getBrojPrimeraka(), knjiga.getIsbn());

		sql = "DELETE from knjiga_zanr WHERE knjiga_id=?";
		jdbcTemplate.update(sql, knjiga.getIsbn());

		for (Zanr zanr : knjiga.getZanrovi()) {
			sql = "INSERT INTO knjiga_zanr(knjiga_id, zanr_id) " + "VALUES('" + knjiga.getIsbn() + "','" + zanr.getId()
					+ "')";
			jdbcTemplate.update(sql);
		}
	}

	@Override
	public void delete(Knjiga knjiga) {
		String sql = "DELETE from knjiga WHERE isbn=?";
		jdbcTemplate.update(sql, knjiga.getIsbn());

	}

}
