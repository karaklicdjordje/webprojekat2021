package web.knjizara.knjiga;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Lob;

import web.knjizara.kupljenaknjiga.KupljenaKnjiga;
import web.knjizara.zanr.Zanr;

public class Knjiga {
     
    private String isbn;
     
    private String naziv;
     
    private String izdavackaKuca;
    
    private String autor;
    
    private Integer godinaIzdavanja;
     
    private String kratakOpis;
    @Lob
    private byte[] slika;
    
    private Double cena;
    
    private Integer brojStranica;
    
    private Povez povez;
    
    private Pismo pismo;
    
    private String jezik;
    
    private Double prosecnaOcenaKnjige;
    
    private Integer brojPrimeraka;
    
    private String slikaKnjige;

    private Set<Zanr> zanrovi = new HashSet<>();
    
	private Set<KupljenaKnjiga> kupljeneKnjige = new HashSet<>();
    
	public Knjiga(String isbn, String naziv, String izdavackaKuca, String autor, Integer godinaIzdavanja,
			String kratakOpis, byte[] slika, Double cena, Integer brojStranica, Povez povez, Pismo pismo, String jezik,
			Double prosecnaOcenaKnjige, Integer brojPrimeraka, Set<Zanr> zanrovi, Set<KupljenaKnjiga> kupljeneKnjige) {
		super();
		this.isbn = isbn;
		this.naziv = naziv;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.godinaIzdavanja = godinaIzdavanja;
		this.kratakOpis = kratakOpis;
		this.slika = slika;
		this.cena = cena;
		this.brojStranica = brojStranica;
		this.povez = povez;
		this.pismo = pismo;
		this.jezik = jezik;
		this.prosecnaOcenaKnjige = prosecnaOcenaKnjige;
		this.brojPrimeraka = brojPrimeraka;
		this.zanrovi = zanrovi;
	}
	
	public Knjiga() {
		super();
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getIzdavackaKuca() {
		return izdavackaKuca;
	}

	public void setIzdavackaKuca(String izdavackaKuca) {
		this.izdavackaKuca = izdavackaKuca;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Integer getGodinaIzdavanja() {
		return godinaIzdavanja;
	}

	public void setGodinaIzdavanja(Integer godinaIzdavanja) {
		this.godinaIzdavanja = godinaIzdavanja;
	}

	public String getKratakOpis() {
		return kratakOpis;
	}

	public void setKratakOpis(String kratakOpis) {
		this.kratakOpis = kratakOpis;
	}
	
	

	



	public String getSlikaKnjige() {
		return slikaKnjige;
	}

	public void setSlikaKnjige(String slikaKnjige) {
		this.slikaKnjige = slikaKnjige;
	}

	public byte[] getSlika() {
		return slika;
	}

	public void setSlika(byte[] slika) {
		this.slika = slika;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Integer getBrojStranica() {
		return brojStranica;
	}

	public void setBrojStranica(Integer brojStranica) {
		this.brojStranica = brojStranica;
	}

	public Povez getPovez() {
		return povez;
	}

	public void setPovez(Povez povez) {
		this.povez = povez;
	}

	public Pismo getPismo() {
		return pismo;
	}

	public void setPismo(Pismo pismo) {
		this.pismo = pismo;
	}

	public String getJezik() {
		return jezik;
	}

	public void setJezik(String jezik) {
		this.jezik = jezik;
	}

	public Double getProsecnaOcenaKnjige() {
		return prosecnaOcenaKnjige;
	}

	public void setProsecnaOcenaKnjige(Double prosecnaOcenaKnjige) {
		this.prosecnaOcenaKnjige = prosecnaOcenaKnjige;
	}

	public Integer getBrojPrimeraka() {
		return brojPrimeraka;
	}

	public void setBrojPrimeraka(Integer brojPrimeraka) {
		this.brojPrimeraka = brojPrimeraka;
	}

	public void setZanrovi(Set<Zanr> zanrovi) {
		zanrovi.clear();
		this.zanrovi = zanrovi;
	}

	public Set<Zanr> getZanrovi() {
		return zanrovi;
	}

	public Set<KupljenaKnjiga> getKupljeneKnjige() {
		return kupljeneKnjige;
	}

	public void setKupljeneKnjige(Set<KupljenaKnjiga> kupljeneKnjige) {
		kupljeneKnjige.clear();
		this.kupljeneKnjige = kupljeneKnjige;
	}

    	
}
