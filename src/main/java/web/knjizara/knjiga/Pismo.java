package web.knjizara.knjiga;

public enum Pismo {
	
	LATINICA, CIRILICA;
	
	public static Pismo vratiPismoByInt(int broj) {
		switch (broj) {
		case 0:
			 return Pismo.LATINICA;
		case 1:
			return Pismo.CIRILICA;
		default:
			return null;
		}
	}

}
