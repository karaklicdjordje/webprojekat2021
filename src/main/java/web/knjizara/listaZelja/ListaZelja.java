package web.knjizara.listaZelja;

import web.knjizara.knjiga.Knjiga;
import web.knjizara.korisnik.Korisnik;


public class ListaZelja {

	private Knjiga knjiga;
	
	private Korisnik korisnik;

	public ListaZelja(Knjiga knjiga, Korisnik korisnik) {
		this.knjiga = knjiga;
		this.korisnik = korisnik;
	}
	
	public ListaZelja() {
		
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	
	
	
}

