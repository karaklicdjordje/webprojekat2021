package web.knjizara.listaZelja;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import web.knjizara.dao.ListaZeljaDAO;
import web.knjizara.knjiga.Knjiga;
import web.knjizara.knjiga.KnjigaRepository;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;

@Repository
public class ListaZeljaRepository implements ListaZeljaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;

	private class UserRowMapper implements RowMapper<ListaZelja> {

		@Override
		public ListaZelja mapRow(ResultSet rs, int rowNum) throws SQLException {

			String knjigaId = rs.getString("knjiga_id");
			Knjiga knjiga = knjigaRepository.findById(knjigaId).get();
			Long korisnikId = rs.getLong("korisnik_id");
			Korisnik korisnik = korisnikRepository.findById(korisnikId).get();
			ListaZelja listaZelja = new ListaZelja();
			listaZelja.setKnjiga(knjiga);
			listaZelja.setKorisnik(korisnik);
			return listaZelja;
		}

	}

	@Override
	public Optional<ListaZelja> findById(ListaZelja listaZelja) {
		try {
			String sql = "SELECT * FROM lista_zelja WHERE knjiga_id=? AND korisnik_id=?";
			ListaZelja listaZeljaIzBaze = jdbcTemplate
					.query(sql, new UserRowMapper(), listaZelja.getKnjiga().getIsbn(), listaZelja.getKorisnik().getId()).get(0);
			return Optional.ofNullable(listaZeljaIzBaze);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<ListaZelja> findAll() {
		String sql = "SELECT * FROM lista_zelja";
		return jdbcTemplate.query(sql, new UserRowMapper());
	}

	public List<ListaZelja> findAllByKorisnikId(Long korisnikId) {
		String sql = "SELECT * FROM lista_zelja WHERE korisnik_id=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), korisnikId);
	}

	public List<ListaZelja> findAllByKnjigaId(String knjigaId) {
		String sql = "SELECT * FROM lista_zelja WHERE knjiga_id=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), knjigaId);
	}

	@Override
	public void save(ListaZelja listaZelja) {
		String sql = "INSERT INTO lista_zelja(knjiga_id, korisnik_id) " + "VALUES('"
				+ listaZelja.getKnjiga().getIsbn() + "','" + listaZelja.getKorisnik().getId() + "')";
		jdbcTemplate.update(sql);
	}

	@Override
	public void update(ListaZelja listaZelja) {

	}

	@Transactional
	@Override
	public void delete(ListaZelja listaZelja) {
		String sql = "DELETE from lista_zelja WHERE korisnik_id=?";
		jdbcTemplate.update(sql, listaZelja.getKorisnik().getId());
	}

	@Transactional
	@Override
	public void deleteByKorisnikIdAndKnjigaId(ListaZelja listaZelja) {
		String sql = "DELETE from lista_zelja WHERE korisnik_id=? AND knjiga_id=?";
		jdbcTemplate.update(sql, listaZelja.getKorisnik().getId(), listaZelja.getKnjiga().getIsbn());
	}

	@Override
	public List<ListaZelja> findByKorisnik(Korisnik korisnik) {
		String sql = "SELECT * FROM lista_zelja WHERE korisnik_id=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), korisnik.getId());
	}

	public ListaZelja findByKnjigaAndKorisnikId(String knjiga_id, Long korisnik_id) {
		String sql = "SELECT * FROM lista_zelja WHERE knjiga_id=? AND korisnik_id=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), knjiga_id, korisnik_id).get(0);
	}

}
