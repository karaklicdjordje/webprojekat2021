package web.knjizara.listaZelja;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import web.knjizara.knjiga.Knjiga;
import web.knjizara.knjiga.KnjigaController;
import web.knjizara.knjiga.KnjigaRepository;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;
import web.knjizara.korpa.KorpaRepository;
import web.knjizara.loyaltyKartica.LoyaltyKartica;
import web.knjizara.loyaltyKartica.LoyaltyKarticaRepository;

@Controller
public class ListaZeljaController {

	@Autowired
	private ListaZeljaRepository listaZeljaRepository;

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;

	@GetMapping("/listaZelja")
	public String prikaziListuKnjigaUListiZelja(Model model) {
		Long korisnikId = korisnikRepository.getKorisnikIdByKorisnikDetails();
		List<ListaZelja> listaZelja = listaZeljaRepository.findAllByKorisnikId(korisnikId);
		model.addAttribute("listaZelja", listaZelja);

		return "lista_zelja";
	}

	@PostMapping("/listaZelja")
	public String ubaciUListuZelja(Model model, ListaZelja listaZelja, String knjiga_id, Long korisnik_id) {
		Knjiga knjiga = knjigaRepository.findById(knjiga_id).get();
		Korisnik korisnik = korisnikRepository.findById(korisnik_id).get();
		listaZelja.setKnjiga(knjiga);
		listaZelja.setKorisnik(korisnik);
		
		if (listaZeljaRepository.findById(listaZelja) != null) {
			return prikaziListuKnjigaUListiZelja(model);
		}

			listaZeljaRepository.save(listaZelja);

		return prikaziListuKnjigaUListiZelja(model);

	}

	@GetMapping("/listaZelja/brisanje")
	public void obrisiIzListeZelja(@RequestParam(value = "korisnikId") Long korisnik_id, @RequestParam(value = "knjigaId") String knjiga_id) {
		ListaZelja listaZelja = listaZeljaRepository.findByKnjigaAndKorisnikId(knjiga_id, korisnik_id);
		listaZeljaRepository.deleteByKorisnikIdAndKnjigaId(listaZelja);
	
	}

}