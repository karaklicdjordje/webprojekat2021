package web.knjizara.korpa;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import web.knjizara.dao.KorpaDAO;
import web.knjizara.knjiga.Knjiga;
import web.knjizara.knjiga.KnjigaRepository;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;

@Repository
public class KorpaRepository implements KorpaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;

	private class UserRowMapper implements RowMapper<Korpa> {

		@Override
		public Korpa mapRow(ResultSet rs, int rowNum) throws SQLException {

			String knjigaId = rs.getString("knjiga_id");
			Knjiga knjiga = knjigaRepository.findById(knjigaId).get();
			Long korisnikId = rs.getLong("korisnik_id");
			Korisnik korisnik = korisnikRepository.findById(korisnikId).get();
			Double cena = rs.getDouble("cena");
			Integer brojPrimeraka = rs.getInt("broj_primeraka");
			Korpa korpa = new Korpa();
			korpa.setKnjiga(knjiga);
			korpa.setKorisnik(korisnik);
			korpa.setCena(cena);
			korpa.setBrojPrimeraka(brojPrimeraka);
			return korpa;
		}

	}

	@Override
	public Optional<Korpa> findById(Korpa korpa) {
		try {
			String sql = "SELECT * FROM korpa WHERE knjiga_id=? AND korisnik_id=?";
			Korpa korpaIzBaze = jdbcTemplate
					.query(sql, new UserRowMapper(), korpa.getKnjiga().getIsbn(), korpa.getKorisnik().getId()).get(0);
			return Optional.ofNullable(korpaIzBaze);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Korpa> findAll() {
		String sql = "SELECT * FROM korpa";
		return jdbcTemplate.query(sql, new UserRowMapper());
	}

	public List<Korpa> findAllByKorisnikId(Long korisnikId) {
		String sql = "SELECT * FROM korpa WHERE korisnik_id=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), korisnikId);
	}

	public List<Korpa> findAllByKnjigaId(String knjigaId) {
		String sql = "SELECT * FROM korpa WHERE knjiga_id=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), knjigaId);
	}

	@Override
	public void save(Korpa korpa) {
		String sql = "INSERT INTO korpa(knjiga_id, korisnik_id, cena, broj_primeraka) " + "VALUES('"
				+ korpa.getKnjiga().getIsbn() + "','" + korpa.getKorisnik().getId() + "','" + korpa.getCena() + "','"
				+ korpa.getBrojPrimeraka() + "')";
		jdbcTemplate.update(sql);
	}

	@Override
	public void update(Korpa korpa) {
		String sql = "UPDATE korpa SET cena = ?, broj_primeraka =? WHERE knjiga_id=? AND korisnik_id=?";
		jdbcTemplate.update(sql, korpa.getCena(), korpa.getBrojPrimeraka(), korpa.getKnjiga().getIsbn(),
				korpa.getKorisnik().getId());
	}

	@Transactional
	@Override
	public void delete(Korpa korpa) {
		String sql = "DELETE from korpa WHERE korisnik_id=?";
		jdbcTemplate.update(sql, korpa.getKorisnik().getId());
	}

	@Transactional
	@Override
	public void deleteByKorisnikIdAndKnjigaId(Korpa korpa) {
		String sql = "DELETE from korpa WHERE korisnik_id=? AND knjiga_id=?";
		jdbcTemplate.update(sql, korpa.getKorisnik().getId(), korpa.getKnjiga().getIsbn());
	}

	@Override
	public List<Korpa> findByKorisnik(Korisnik korisnik) {
		String sql = "SELECT * FROM korpa WHERE korisnik_id=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), korisnik.getId());
	}

	public Korpa findByKnjigaAndKorisnikId(String knjiga_id, Long korisnik_id) {
		String sql = "SELECT * FROM korpa WHERE knjiga_id=? AND korisnik_id=?";
		return jdbcTemplate.query(sql, new UserRowMapper(), knjiga_id, korisnik_id).get(0);
	}

}
