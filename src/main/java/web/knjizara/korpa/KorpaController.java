package web.knjizara.korpa;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import web.knjizara.knjiga.Knjiga;
import web.knjizara.knjiga.KnjigaController;
import web.knjizara.knjiga.KnjigaRepository;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;
import web.knjizara.loyaltyKartica.LoyaltyKartica;
import web.knjizara.loyaltyKartica.LoyaltyKarticaRepository;

@Controller
public class KorpaController {

	@Autowired
	private KorpaRepository korpaRepository;

	@Autowired
	private KnjigaRepository knjigaRepository;

	@Autowired
	private KorisnikRepository korisnikRepository;

	@Autowired
	private KnjigaController knjigaController;

	@Autowired
	private LoyaltyKarticaRepository loyaltyKarticaRepository;

	@GetMapping("/korpa")
	public String prikaziListuKnjiga(Model model) {
		Long korisnikId = korisnikRepository.getKorisnikIdByKorisnikDetails();
		List<Korpa> listaKorpa = korpaRepository.findAllByKorisnikId(korisnikId);
		LoyaltyKartica loyaltyKartica = loyaltyKarticaRepository.findByKorisnikId(korisnikId);
		model.addAttribute("listaKorpa", listaKorpa);
		model.addAttribute("loyalty", loyaltyKartica);

		Map<String, Integer> mapaPopust = new LinkedHashMap<String, Integer>();

		if (loyaltyKartica != null) {
			Integer popustLimit = 10;
			if (loyaltyKartica.getBrojPoena() < popustLimit) {
				popustLimit = loyaltyKartica.getBrojPoena();
			}
			for (Integer i = 1; i <= popustLimit; i++) {

				String popust = "" + (i * 5) + "% (" + i + " poena)";
				if (i == 1) {
					popust = "" + (i * 5) + "% (" + i + " poen)";
				}
				mapaPopust.put(popust, i);
			}
		}
		model.addAttribute("mapaPopust", mapaPopust);
		return "korpa";
	}

	@PostMapping("/korpa")
	public String ubaciUKorpu(Model model, Korpa korpa, String knjiga_id, Long korisnik_id) {
		Knjiga knjiga = knjigaRepository.findById(knjiga_id).get();
		Korisnik korisnik = korisnikRepository.findById(korisnik_id).get();
		korpa.setKnjiga(knjiga);
		korpa.setKorisnik(korisnik);

		if (korpaRepository.findById(korpa) != null) {
			Korpa korpaIzBaze = korpaRepository.findById(korpa).get();

			int brojPrimerakaKnjige = korpa.getBrojPrimeraka() + korpaIzBaze.getBrojPrimeraka();
			if (brojPrimerakaKnjige > knjiga.getBrojPrimeraka()) {
				String errorMessage = "Nema dovoljno knjiga na stanju. U vasoj korpi se trenutno nalazi "
						+ korpaIzBaze.getBrojPrimeraka() + " primeraka knjige " + knjiga.getNaziv()
						+ ". Maksimalan broj primeraka koji mozete uneti je "
						+ (knjiga.getBrojPrimeraka() - korpaIzBaze.getBrojPrimeraka() + ".");
				model.addAttribute("errorMessage", errorMessage);

				return knjigaController.prikaziKnjigu(model, knjiga_id);

			} else {
				if (knjiga_id.equals(korpaIzBaze.getKnjiga().getIsbn())) {
					korpa.setBrojPrimeraka(korpaIzBaze.getBrojPrimeraka() + korpa.getBrojPrimeraka());
					korpa.setCena(korpaIzBaze.getCena() + korpa.getCena());
					korpaRepository.update(korpa);
				}
			}
		} else {
			korpaRepository.save(korpa);
		}

		return "uspesno_ubacivanje_u_korpu";

	}

	@GetMapping("/korpa/brisanje")
	public void ubaciUKorpu(@RequestParam(value = "korisnikId") Long korisnik_id, @RequestParam(value = "knjigaId") String knjiga_id) {
		Korpa korpa = korpaRepository.findByKnjigaAndKorisnikId(knjiga_id, korisnik_id);
		korpaRepository.deleteByKorisnikIdAndKnjigaId(korpa);
	
	}

}