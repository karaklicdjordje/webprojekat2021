package web.knjizara.korpa;

import web.knjizara.knjiga.Knjiga;
import web.knjizara.korisnik.Korisnik;


public class Korpa {
	
	private Integer brojPrimeraka;
	
	private Double cena;
	
	private Knjiga knjiga;
	
	private Korisnik korisnik;

	public Korpa(Integer brojPrimeraka, Double cena, Knjiga knjiga, Korisnik korisnik) {
		this.brojPrimeraka = brojPrimeraka;
		this.cena = cena;
		this.knjiga = knjiga;
		this.korisnik = korisnik;
	}
	
	public Korpa() {
		
	}

	public Integer getBrojPrimeraka() {
		return brojPrimeraka;
	}

	public void setBrojPrimeraka(Integer brojPrimeraka) {
		this.brojPrimeraka = brojPrimeraka;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	
	
	
}

