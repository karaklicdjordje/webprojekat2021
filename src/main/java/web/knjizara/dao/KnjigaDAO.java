package web.knjizara.dao;

import web.knjizara.knjiga.Knjiga;

public interface KnjigaDAO extends Dao<Knjiga, String>{
	
}
