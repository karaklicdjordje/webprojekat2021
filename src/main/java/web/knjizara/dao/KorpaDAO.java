package web.knjizara.dao;

import java.util.List;

import web.knjizara.korisnik.Korisnik;
import web.knjizara.korpa.Korpa;

public interface KorpaDAO extends Dao<Korpa, Korpa>{

	public List<Korpa> findByKorisnik(Korisnik korisnik);
	
	public void deleteByKorisnikIdAndKnjigaId(Korpa korpa);
	
}
