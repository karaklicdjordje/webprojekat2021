package web.knjizara.dao;

import java.util.List;
import java.util.Optional;

public interface Dao<T,S> {
	
	Optional<T> findById(S id);
    
    List<T> findAll();
    
    void save(T t);
    
    void update(T t);
    
    void delete(T t);

}
