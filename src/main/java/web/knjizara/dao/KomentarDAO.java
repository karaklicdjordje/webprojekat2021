package web.knjizara.dao;

import java.util.List;

import web.knjizara.komentar.Komentar;
import web.knjizara.komentar.StatusKomentara;

public interface KomentarDAO extends Dao<Komentar, Long>{
	
	public List<Komentar> findAllByStatusKomentara(StatusKomentara statusKomentara);
	
	public Komentar findByKorisnikId(Long korisnikId);
	
}
