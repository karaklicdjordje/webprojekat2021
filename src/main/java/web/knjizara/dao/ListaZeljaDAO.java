package web.knjizara.dao;

import java.util.List;

import web.knjizara.korisnik.Korisnik;
import web.knjizara.korpa.Korpa;
import web.knjizara.listaZelja.ListaZelja;

public interface ListaZeljaDAO extends Dao<ListaZelja, ListaZelja>{

	public List<ListaZelja> findByKorisnik(Korisnik korisnik);
	
	public void deleteByKorisnikIdAndKnjigaId(ListaZelja listaZelja);
	
}
