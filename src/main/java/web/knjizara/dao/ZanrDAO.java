package web.knjizara.dao;

import web.knjizara.zanr.Zanr;

public interface ZanrDAO extends Dao<Zanr, Long>{

}
