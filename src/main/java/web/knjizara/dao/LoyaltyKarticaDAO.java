package web.knjizara.dao;

import java.util.List;

import web.knjizara.loyaltyKartica.LoyaltyKartica;
import web.knjizara.loyaltyKartica.StatusKartice;

public interface LoyaltyKarticaDAO extends Dao<LoyaltyKartica, Long>{
	
	public List<LoyaltyKartica> findAllByStatusKartice(StatusKartice statusKartice);
	
	public LoyaltyKartica findByKorisnikId(Long korisnikId);
	
}
