package web.knjizara.dao;

import web.knjizara.korisnik.Korisnik;

public interface KorisnikDAO extends Dao<Korisnik, Long> {

	Korisnik findByKorisnickoIme(String korisnickoIme);
	
	Korisnik findKorisnikByKorisnikDetails();
	
	Long getKorisnikIdByKorisnikDetails();
	
	public boolean hasLoyaltyKartica(Long korisnikId);
	
	public boolean isLoyaltyKarticaNaCekanju(Long korisnikId);

}
