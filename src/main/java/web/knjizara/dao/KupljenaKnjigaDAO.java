package web.knjizara.dao;

import java.time.LocalDateTime;
import java.util.List;

import web.knjizara.kupljenaknjiga.KupljenaKnjiga;
import web.knjizara.kupovina.Kupovina;

public interface KupljenaKnjigaDAO extends Dao<KupljenaKnjiga, String>{

	List<KupljenaKnjiga> findAllByVremeKupovine(LocalDateTime startDate, LocalDateTime endDate);

	List<KupljenaKnjiga> findAllByKupovinaId(Kupovina kupovinaId);

	List<KupljenaKnjiga> findAllByKupovinaId(Long kupovinaId);

	List<KupljenaKnjiga> findAllByKupovinaVremeKupovine(LocalDateTime dateTime);

}
