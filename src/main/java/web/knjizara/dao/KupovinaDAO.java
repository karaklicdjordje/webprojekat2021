package web.knjizara.dao;

import java.util.List;

import web.knjizara.kupovina.Kupovina;

public interface KupovinaDAO extends Dao<Kupovina, Long> {

	List<Kupovina> findAllByKorisnikId(Long korisnikId);

}
