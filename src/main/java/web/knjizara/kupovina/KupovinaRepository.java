package web.knjizara.kupovina;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import web.knjizara.dao.KupovinaDAO;
import web.knjizara.korisnik.Korisnik;
import web.knjizara.korisnik.KorisnikRepository;

@Repository
public class KupovinaRepository implements KupovinaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private KorisnikRepository korisnikRepository;

	private class KupovinaRowMapper implements RowMapper<Kupovina> {

		private Map<Long, Kupovina> kupovinaMap = new LinkedHashMap<>();

		@Override
		public Kupovina mapRow(ResultSet rs, int rowNum) throws SQLException {

			Long kupovinaId = rs.getLong("kupovina_id");

			Long korisnikId = rs.getLong("korisnik_id");
			Korisnik korisnik = korisnikRepository.findById(korisnikId).get();
			Double ukupnaCenaKupovine = rs.getDouble("ukupna_cena_kupovine");
			Integer brojKupljenihKnjiga = rs.getInt("broj_kupljenih_knjiga");
			Timestamp vremeKupovineSql = rs.getTimestamp("vreme_kupovine");
			LocalDateTime vremeKupovine = vremeKupovineSql.toLocalDateTime();
			Kupovina kupovina = new Kupovina();
			kupovina.setKupovinaId(kupovinaId);
			kupovina.setBrojKupljenihKnjiga(brojKupljenihKnjiga);
			kupovina.setUkupnaCenaKupovina(ukupnaCenaKupovine);
			kupovina.setVremeKupovine(vremeKupovine);
			kupovina.setKorisnik(korisnik);
			kupovinaMap.put(kupovinaId, kupovina);
			return kupovina;
		}

		public List<Kupovina> vratiKupovine() {
			return new ArrayList<>(kupovinaMap.values());

		}

	}

	@Override
	public Optional<Kupovina> findById(Long id) {
		String sql = "SELECT * FROM kupovina WHERE kupovina_id=?";

		KupovinaRowMapper rowCallbackHandler = new KupovinaRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		Kupovina kupovina = rowCallbackHandler.vratiKupovine().get(0);

		return Optional.ofNullable(kupovina);
	}

	@Override
	public List<Kupovina> findAllByKorisnikId(Long korisnikId) {
		try {

			String sql = "SELECT * FROM kupovina WHERE korisnik_id=?";

			KupovinaRowMapper rowCallbackHandler = new KupovinaRowMapper();
			jdbcTemplate.query(sql, rowCallbackHandler, korisnikId);

			return rowCallbackHandler.vratiKupovine();
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Kupovina> findAll() {
		String sql = "SELECT * FROM kupovina";

		KupovinaRowMapper rowCallbackHandler = new KupovinaRowMapper();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.vratiKupovine();
	}

	@Override
	public void save(Kupovina kupovina) {
		String sql = "INSERT INTO kupovina(ukupna_cena_kupovine, korisnik_id, vreme_kupovine, broj_kupljenih_knjiga) "
				+ "VALUES('" + kupovina.getUkupnaCenaKupovina() + "','" + kupovina.getKorisnik().getId() + "','"
				+ Timestamp.valueOf(kupovina.getVremeKupovine()) + "','" + kupovina.getBrojKupljenihKnjiga() + "')";
		jdbcTemplate.update(sql);

	}

	@Override
	public void update(Kupovina t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(Kupovina t) {
		// TODO Auto-generated method stub

	}

}
