package web.knjizara.kupovina;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import web.knjizara.korisnik.Korisnik;
import web.knjizara.kupljenaknjiga.KupljenaKnjiga;

public class Kupovina implements Comparable<Kupovina>{

	private Long kupovinaId;
	
	private double ukupnaCenaKupovina;
	
	private LocalDateTime vremeKupovine;
	
	private Korisnik korisnik;
	
	private int brojKupljenihKnjiga;
	
	private Set<KupljenaKnjiga> kupljenaKnjiga = new HashSet<>();

	
	public Kupovina() {
		
	}

	public Kupovina(Long kupovinaId, double ukupnaCenaKupovina, LocalDateTime vremeKupovine, Korisnik korisnik,
			int brojKupljenihKnjiga) {
		super();
		this.kupovinaId = kupovinaId;
		this.ukupnaCenaKupovina = ukupnaCenaKupovina;
		this.vremeKupovine = vremeKupovine;
		this.korisnik = korisnik;
		this.brojKupljenihKnjiga = brojKupljenihKnjiga;
	}

	public Long getKupovinaId() {
		return kupovinaId;
	}

	public void setKupovinaId(Long kupovinaId) {
		this.kupovinaId = kupovinaId;
	}

	public double getUkupnaCenaKupovina() {
		return ukupnaCenaKupovina;
	}

	public void setUkupnaCenaKupovina(double ukupnaCenaKupovina) {
		this.ukupnaCenaKupovina = ukupnaCenaKupovina;
	}

	public LocalDateTime getVremeKupovine() {
		return vremeKupovine;
	}

	public void setVremeKupovine(LocalDateTime vremeKupovine) {
		this.vremeKupovine = vremeKupovine;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public int getBrojKupljenihKnjiga() {
		return brojKupljenihKnjiga;
	}

	public void setBrojKupljenihKnjiga(int brojKupljenihKnjiga) {
		this.brojKupljenihKnjiga = brojKupljenihKnjiga;
	}
	
	public Set<KupljenaKnjiga> getKupljenaKnjiga() {
		return this.kupljenaKnjiga;
	}
	
	public void addKupljenaKnjiga(KupljenaKnjiga kupljenaKnjiga) {
		this.kupljenaKnjiga.add(kupljenaKnjiga);
	}

	@Override
	public int compareTo(Kupovina drugaKupovina) {
		return drugaKupovina.vremeKupovine.compareTo(this.vremeKupovine);
	}
	
	
	
}
