package web.knjizara.kupovina;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import web.knjizara.korisnik.Korisnik;
import web.knjizara.kupljenaknjiga.KupljenaKnjiga;
import web.knjizara.kupljenaknjiga.KupljenaKnjigaRepository;

@Controller
public class KupovinaController {

	@Autowired
	KupovinaRepository kupovinaRepository;

	@Autowired
	private KupljenaKnjigaRepository kupljenaKnjigaRepository;

	@GetMapping("/admin/prikaziKupovine")
	public String vratiKupovine(Model model) {
		if (model.getAttribute("kupljeneKnjige") == null) {
			List<KupljenaKnjiga> kupljeneKnjige = new ArrayList<>();
			model.addAttribute("kupljeneKnjige", kupljeneKnjige);
		}

		return "prikazi_kupovine";
	}

	@PostMapping("/admin/prikaziKupovine")
	public String vratiKupovineFilter(Model model, String startDate, String endDate) {

		startDate += " 00:00";
		endDate += " 23:59";
		List<KupljenaKnjiga> kupljeneKnjigeModel = new ArrayList<>();
		String pocetak = null;
		String kraj = null;
		double ukupnaCenaKnjiga = 0.0;
		int ukupanBrojPrimerakaKnjiga = 0;

		if ((startDate != null && !startDate.isEmpty()) && (endDate != null && !endDate.isEmpty())) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
			LocalDateTime startDateTime = LocalDateTime.parse(startDate, formatter);
			LocalDateTime endDateTime = LocalDateTime.parse(endDate, formatter);

			kupljeneKnjigeModel = kupljenaKnjigaRepository.findAllByVremeKupovine(startDateTime, endDateTime);

			pocetak = startDate.substring(0, 10);
			kraj = endDate.substring(0, 10);
		} else {
			kupljeneKnjigeModel = kupljenaKnjigaRepository.findAll();

		}

		for (KupljenaKnjiga kupljenaKnjiga : kupljeneKnjigeModel) {
			ukupanBrojPrimerakaKnjiga += kupljenaKnjiga.getUkupanBrojKnjiga();
			double cenaDveDecimale = Math.round(kupljenaKnjiga.getUkupnaCena() * 100.0d) / 100.0d;
			kupljenaKnjiga.setUkupnaCena(cenaDveDecimale);
			ukupnaCenaKnjiga += kupljenaKnjiga.getUkupnaCena();
		}
		
		String ukupnaCenaKnjigaString = String.format("%.2f", ukupnaCenaKnjiga);

		model.addAttribute("kupljeneKnjige", kupljeneKnjigeModel);
		model.addAttribute("ukupanBrojPrimerakaKnjiga", ukupanBrojPrimerakaKnjiga);
		model.addAttribute("ukupnaCenaKnjiga", ukupnaCenaKnjigaString);
		model.addAttribute("pocetak", pocetak);
		model.addAttribute("kraj", kraj);

		return vratiKupovine(model);
	}

	@GetMapping("/knjige/prikaziKupovine/{korisnikId}")
	public String prikaziKupovineKorisnikaAdmin(Model model,@PathVariable Long korisnikId) {

		List<Kupovina> kupovine = kupovinaRepository.findAllByKorisnikId(korisnikId);
		Collections.sort(kupovine);
		model.addAttribute("kupovine", kupovine);

		return "prikazi_kupovine_korisnika";

	}
	
	
	@GetMapping("/knjige/kupovina/{kupovinaId}")
	public String prikaziKnjigeKupovineKorisnikaAdmin(Model model, @PathVariable Long kupovinaId) {
		double ukupnaCenaKnjiga = 0.0;
		int ukupanBrojPrimerakaKnjiga = 0;
		
		Kupovina kupovina = kupovinaRepository.findById(kupovinaId).get();	
		List<KupljenaKnjiga> kupljeneKnjige = kupljenaKnjigaRepository.findAllByKupovinaVremeKupovine(kupovina.getVremeKupovine());
		
		for (KupljenaKnjiga kupljenaKnjiga : kupljeneKnjige) {
			ukupanBrojPrimerakaKnjiga += kupljenaKnjiga.getUkupanBrojKnjiga();
			double cenaDveDecimale = Math.round(kupljenaKnjiga.getUkupnaCena() * 100.0d) / 100.0d;
			kupljenaKnjiga.setUkupnaCena(cenaDveDecimale);
			ukupnaCenaKnjiga += kupljenaKnjiga.getUkupnaCena();
		}
		
		String ukupnaCenaKnjigaString = String.format("%.2f", ukupnaCenaKnjiga);
		
		model.addAttribute("kupovina", kupovina);
		model.addAttribute("kupljeneKnjige", kupljeneKnjige);
		model.addAttribute("ukupanBrojPrimerakaKnjiga", ukupanBrojPrimerakaKnjiga);
		model.addAttribute("ukupnaCenaKnjiga", ukupnaCenaKnjigaString);

		return "prikazi_knjige_kupovina";

	}

}
