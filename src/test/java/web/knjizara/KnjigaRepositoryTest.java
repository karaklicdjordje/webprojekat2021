package web.knjizara;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import web.knjizara.knjiga.Knjiga;
import web.knjizara.knjiga.KnjigaRepository;
import web.knjizara.knjiga.Pismo;
import web.knjizara.knjiga.Povez;
import web.knjizara.zanr.Zanr;
import web.knjizara.zanr.ZanrRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace=Replace.NONE)
@Rollback(false)
public class KnjigaRepositoryTest {
	
	@Autowired
	private KnjigaRepository knjigaRepository;
	
	@Autowired
	private ZanrRepository zanrRepository;
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Test
	public void testKreirajKnjigu() {
		Zanr zanr = new Zanr();
		zanr.setIme("epika");
		zanr.setOpis("Kazivanje o nekom dogadjaju, zbivanju ili licnosti");
		zanrRepository.save(zanr);
		
		Knjiga knjiga = new Knjiga();
		knjiga.setAutor("Mark Tven");
		knjiga.setBrojStranica(354);
		knjiga.setCena(544.45);
		knjiga.setGodinaIzdavanja(1954);
		knjiga.setIsbn("1234567890123");
		knjiga.setIzdavackaKuca("Nolit");
		knjiga.setJezik("engleski");
		knjiga.setKratakOpis("Dozivljaji decaka Tom Sojera i njegovog druga Haklberi Fina");
		knjiga.setNaziv("Tom Sojer");
		knjiga.setPismo(Pismo.LATINICA);
		knjiga.setPovez(Povez.MEKI);
		double prosecnaOcena = (Math.random() * 4 + 1);
		prosecnaOcena = Math.round(prosecnaOcena*100.0)/100.0;
		knjiga.setProsecnaOcenaKnjige(prosecnaOcena);
		
		knjiga.getZanrovi().add(zanr);
		
		knjigaRepository.save(knjiga);
		
		Knjiga knjigaIzBaze = entityManager.find(Knjiga.class, knjiga.getIsbn());
		
		assertThat(knjigaIzBaze.getNaziv().equals(knjiga.getNaziv()));		
		
	}
	
}
