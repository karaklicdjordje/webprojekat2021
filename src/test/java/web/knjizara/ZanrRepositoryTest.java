package web.knjizara;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.Rollback;

import web.knjizara.zanr.Zanr;
import web.knjizara.zanr.ZanrRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class ZanrRepositoryTest {

	@Autowired
	private ZanrRepository zanrRepository;

	@Autowired
	private TestEntityManager entityManager;

	@Test
	public void testKreirajZanr() {
		Zanr zanr = new Zanr();
		zanr.setIme("epika");
		zanr.setOpis("Kazivanje o nekom dogadjaju, zbivanju ili licnosti");

		zanrRepository.save(zanr);
		Zanr zanrIzBaze = entityManager.find(Zanr.class, zanr.getId());

		assertThat(zanrIzBaze.getIme().equals(zanr.getIme()));
	}

}
